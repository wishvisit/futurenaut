
---Downloaded files---
The import files for: The Landscaper were successfully downloaded!
Initial max execution time = 120
Files info:
Site URL = https://wordpress-241359-941743.cloudwaysapps.com
Data file = /home/241359.cloudwaysapps.com/engmfgphxv/public_html/wp-content/themes/the-landscaper/demo-files/content.xml
Widget file = /home/241359.cloudwaysapps.com/engmfgphxv/public_html/wp-content/themes/the-landscaper/demo-files/widgets.json
Customizer file = /home/241359.cloudwaysapps.com/engmfgphxv/public_html/wp-content/themes/the-landscaper/demo-files/customizer.dat
Redux files:
not defined!

---Importing widgets---
Blog Sidebar : 

Meta - No Title - Imported
Visual Editor - The Landscaper - Imported
QT: Brochure - Brochures - Imported
Tag Cloud - Tag Cloud - Imported
QT: Facebook - No Title - Imported

Topbar : 

QT: Icon Box - 202 Park Avenue, New York - Imported
QT: Icon Box - +123 - 777 - 456 - 789 - Imported
QT: Icon Box - Mon-Sat: 07:00 - 17:00 - Imported
QT: Social Icons - No Title - Imported

Page Sidebar : 

Navigation Menu - No Title - Imported
Visual Editor - Have Any Questions? - Imported

shop-sidebar : Sidebar does not exist in theme (moving widget to Inactive)

woocommerce_price_filter - Filter by price - Site does not support widget
woocommerce_top_rated_products - No Title - Site does not support widget
woocommerce_product_search - Search - Site does not support widget
woocommerce_product_categories - Product Categories - Site does not support widget
Visual Editor - Payment - Imported to Inactive

Footer : 

Visual Editor - About Us - Imported
Navigation Menu - Extra Navigation - Imported
Navigation Menu - Our Services - Imported
QT: Opening Hours - Working Hours - Imported



---Importing customizer settings---
Customizer settings import finished!

---pt-ocdi/after_import---

