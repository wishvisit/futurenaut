<?php

/**
 * Topbar settings for the transparent header layout
 */
$wp_customize->add_setting( 'qt_topbar_textcolor_transparent', array(
    'default'     			=> '#b5b5b5',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_topbar_textcolor_transparent', array(
	'label' 				=> esc_html__( 'Topbar text color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the text color of the topbar', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_topbar_textcolor_transparent',
	'active_callback'		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_topbar_icon_color_transparent', array(
    'default'     			=> '#b5b5b5',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_topbar_icon_color_transparent', array(
	'label' 				=> esc_html__( 'Topbar icon color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the icon color of the topbar', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_topbar_icon_color_transparent',
	'active_callback'		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_topbar_icon_color_hover_transparent', array(
    'default'     			=> '#ffffff',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_topbar_icon_color_hover_transparent', array(
	'label' 				=> esc_html__( 'Topbar icon color hover', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the hover icon color of the topbar', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_topbar_icon_color_hover_transparent',
	'active_callback'		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_topbar_link_color_transparent', array(
    'default'     			=> '#b5b5b5',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_topbar_link_color_transparent', array(
	'label' 				=> esc_html__( 'Topbar link color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the link color of the topbar', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_topbar_link_color_transparent',
	'active_callback'		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_topbar_link_hover_color_transparent', array(
    'default'     			=> '#ffffff',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_topbar_link_hover_color_transparent', array(
	'label' 				=> esc_html__( 'Topbar link hover color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the link hover color of the topbar', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_topbar_link_hover_color_transparent',
	'active_callback'		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

/**
 * Header settings for the transparent header layout
 */
$wp_customize->add_setting( 'qt_topbar_bg_transparent', array(
	'default' 				=> '#3a3a3a',
    'transport'				=> 'refresh',
	'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_topbar_bg_transparent', array(
	'label'       			=> esc_html__( 'Transparent header | mobile topbar background color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the mobile topbar background color of the transparent header layout', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_header',
	'settings'    			=> 'qt_topbar_bg_transparent',
	'priority'    			=> 74,
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_gradient_bg_transparent', array(
	'default' 				=> '#3a3a3a',
    'transport'				=> 'refresh',
	'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_gradient_bg_transparent', array(
	'label'       			=> esc_html__( 'Transparent header | background color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the background color of the transparent header layout', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_header',
	'settings'    			=> 'qt_gradient_bg_transparent',
	'priority'    			=> 75,
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_gradient_bg_transparent_opacity', array(
	'default' 				=> '0.5',
	'transport'				=> 'refresh',
	'sanitize_callback' 	=> 'thelandscaper_sanitize_text',
) );
$wp_customize->add_control( 'qt_gradient_bg_transparent_opacity', array(
    'label' 				=> esc_html__( 'Transparent header | background opacity', 'the-landscaper-wp' ),
    'description' 			=> esc_html__( 'Change the background opacity of the transparent header layout. Add a value between 0 and 1 (e.g. 0.5)', 'the-landscaper-wp' ),
	'type'		       	 	=> 'number',
    'section' 				=> 'qt_section_header',
    'settings' 				=> 'qt_gradient_bg_transparent_opacity',
    'priority' 				=> 76,
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) );

/**
 * Navigation settings for the transparent header layout
 */
$wp_customize->add_setting( 'qt_nav_stickynav_bg_transparent', array(
    'default'    			=> '#000000',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_nav_stickynav_bg_transparent', array(
	'label'      			=> esc_html__( 'Sticky nav background color', 'the-landscaper-wp' ),
	'description'			=> esc_html__( 'Change the background color of the sticky navigation', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_navigation',
	'settings'    			=> 'qt_nav_stickynav_bg_transparent',
	'active_callback' 		=> array( $this, 'thelandscaper_show_setting_stickynav_background' ),
) ) );

$wp_customize->add_setting( 'qt_nav_stickynav_bg_transparent_opacity', 
	array(
	'default' 				=> '0.5',
	'transport'				=> 'refresh',
	'sanitize_callback' 	=> 'thelandscaper_sanitize_text',
) );
$wp_customize->add_control( 'qt_nav_stickynav_bg_transparent_opacity', array(
    'label' 				=> esc_html__( 'Sticky nav background opacity', 'the-landscaper-wp' ),
    'description' 			=> esc_html__( 'Add a value between 0 and 1, e.g. 0.5', 'the-landscaper-wp' ),
	'type'		       	 	=> 'number',
    'section' 				=> 'qt_section_navigation',
    'settings' 				=> 'qt_nav_stickynav_bg_transparent_opacity',
	'active_callback' 		=> array( $this, 'thelandscaper_show_setting_stickynav_background' ),
) );

$wp_customize->add_setting( 'qt_nav_textcolor_transparent', array(
    'default'    			=> '#ffffff',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_nav_textcolor_transparent', array(
	'label'      			=> esc_html__( 'Link color', 'the-landscaper-wp' ),
	'description'			=> esc_html__( 'Change the color of the parent links', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_navigation',
	'settings'    			=> 'qt_nav_textcolor_transparent',
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_nav_textcolor_hover_transparent', array(
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_nav_textcolor_hover_transparent', array(
	'label'      			=> esc_html__( 'Link hover color', 'the-landscaper-wp' ),
	'description'			=> esc_html__( 'Change the hover color of the parent links', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_navigation',
	'settings'    			=> 'qt_nav_textcolor_hover_transparent',
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_nav_submenu_bg_transparent', array(
	'default' 				=> '#434343',
	'transport'				=> 'refresh',
	'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_nav_submenu_bg_transparent', array(
	'label'       			=> esc_html__( 'Submenu background color', 'the-landscaper-wp' ),
	'description'			=> esc_html__( 'Change the background color of the submenu\'s', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_navigation',
	'settings'    			=> 'qt_nav_submenu_bg_transparent',
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_nav_submenu_textcolor_transparent', array(
	'default'     			=> '#999999',
	'transport'				=> 'refresh',
	'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_nav_submenu_textcolor_transparent', array(
	'label'       			=> esc_html__( 'Submenu link color', 'the-landscaper-wp' ),
	'description'			=> esc_html__( 'Change the link color of the submenu\'s', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_navigation',
	'settings'    			=> 'qt_nav_submenu_textcolor_transparent',
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );

$wp_customize->add_setting( 'qt_nav_submenu_topline_transparent', array(
	'default'     			=> '#a2c046',
	'transport'				=> 'refresh',
	'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_nav_submenu_topline_transparent', array(
	'label'       			=> esc_html__( 'Submenu top border color', 'the-landscaper-wp' ),
	'description'			=> esc_html__( 'Change the top border color of the submenu\'s', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_navigation',
	'settings'    			=> 'qt_nav_submenu_topline_transparent',
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_transparent' ),
) ) );