<?php

/* Header settings for overlay header layout */
$wp_customize->add_setting( 'qt_header_widgets', array(
	'default'  			=> 'show',
	'transport'			=> 'refresh',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( 'qt_header_widgets', array(
	'label'    			=> esc_html__( 'Header overlay | widgets', 'the-landscaper-wp' ),
	'description' 		=> esc_html__( 'Show or hide the header widget area on mobile screens', 'the-landscaper-wp' ),
	'section'  			=> 'qt_section_header',
	'settings' 			=> 'qt_header_widgets',
	'type'     			=> 'select',
	'choices'  			=> array(
		'show'  		=> esc_html__( 'Show', 'the-landscaper-wp' ),
		'hide_mobile' 	=> esc_html__( 'Hide', 'the-landscaper-wp'),
	),
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_overlay' ),
) );

$wp_customize->add_setting( 'qt_header_background', array( 
	'default' 				=> '#ffffff',
    'transport'				=> 'refresh',
	'sanitize_callback' 	=> 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_header_background', array(
	'label'       			=> esc_html__( 'Header overlay | background color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the background color of the header', 'the-landscaper-wp' ),
	'section'     			=> 'qt_section_header',
	'settings'    			=> 'qt_header_background',
	'priority'    			=> 25,
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_overlay' ),
) ) );

$wp_customize->add_setting( 'qt_header_title_color', array(
    'default'     			=> '#080808',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_header_title_color', array(
	'label' 				=> esc_html__( 'Header overlay | widget title color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the title color of the header', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_header_title_color',
	'priority' 				=> 30,
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_overlay' ),
) ) );

$wp_customize->add_setting( 'qt_header_text_color', array(
    'default'     			=> '#7d7d7d',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_header_text_color', array(
	'label' 				=> esc_html__( 'Header overlay | text color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the text color of the header', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_header_text_color',
	'priority' 				=> 35,
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_overlay' ),
) ) );

$wp_customize->add_setting( 'qt_header_icon_color', array(
    'default'     			=> '#7d7d7d',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_header_icon_color', array(
	'label' 				=> esc_html__( 'Header overlay | icon color', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the icon color of the header', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_header_icon_color',
	'priority' 				=> 40,
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_overlay' ),
) ) );

$wp_customize->add_setting( 'qt_header_icon_color_hover', array(
    'default'     			=> '#7d7d7d',
    'transport'				=> 'refresh',
    'sanitize_callback' 	=> 'sanitize_hex_color'
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'qt_header_icon_color_hover', array(
	'label' 				=> esc_html__( 'Header overlay | icon color hover', 'the-landscaper-wp' ),
	'description' 			=> esc_html__( 'Change the hover icon color of the header', 'the-landscaper-wp' ),
	'section' 				=> 'qt_section_header',
	'settings' 				=> 'qt_header_icon_color_hover',
	'priority' 				=> 45,
	'active_callback' 		=> array( $this, 'thelandscaper_show_settings_header_overlay' ),
) ) );