<?php
/**
 * Filters for the Theme
 *
 * @package The Landscaper
 */

/**
 * Add shortcodes in widgets
 */
add_filter( 'widget_text', 'do_shortcode' );

/**
 * Custom tag font size
 */
if ( ! function_exists( 'thelandscaper_tag_cloud_sizes' ) ) {
    function thelandscaper_tag_cloud_sizes( $args ) {
    	$args['number'] = 12;
    	$args['largest'] = 11;
    	$args['smallest'] = 9;
    	return $args;
    }
    add_filter( 'widget_tag_cloud_args', 'thelandscaper_tag_cloud_sizes' );
}

/**
 * Wrap embed videos in a wrapper
 */
if ( ! function_exists( 'thelandscaper_oembed_html' ) ) {
    function thelandscaper_oembed_html( $html, $url, $attr, $post_id ) {
        return '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
    }
    add_filter( 'embed_oembed_html', 'thelandscaper_oembed_html', 99, 4 );
}

/**
 * Wrap first word of widget title a <span>
 */
if ( ! function_exists( 'thelandscaper_widget_title' ) ) {
    function thelandscaper_widget_title( $title, $instance = '', $id_base = '' ) {

        // Exclude RSS widget
        if ( 'rss' != $id_base ) {

            // Cut the title to 2 parts
            $title_parts = explode( ' ', $title, 2 );

            // Wrap first word in span tag
            if ( ! empty( $title ) ) {
                $title = '<span class="light">' . $title_parts[0] . '</span>';
            
                // Add the rest of the title if exist
                if ( isset( $title_parts[1] ) ) {
                    $title .= ' ' . $title_parts[1];
                }
            }

            // Return title if isset
            if ( $title_parts[0] ) {
                $title = $title;
            }
        }

        return $title;
    }
    add_filter( 'widget_title', 'thelandscaper_widget_title', 10, 3 );
}

/**
 * Get selected Google fonts from the customizer
 */
if ( ! function_exists( 'thelandscaper_get_customizer_fonts' ) ) {
    function thelandscaper_get_customizer_fonts( $fonts ) {

        // Get the customizer font type settings
        $primary_font = get_theme_mod( 'qt_theme_primary_font', 'Roboto' );
        $secondary_font = get_theme_mod( 'qt_theme_secondary_font', 'Roboto Slab' );

        // Get selected fonts and set default font weight (400 is regular)
        $font_primary = array( $primary_font => array( '400', '700' ) );
        $font_secondary = array( $secondary_font => array( '400', '700' ) );

        // Merge everything and remove duplicated
        $fonts = array_merge( $font_primary, $font_secondary );
        $fonts = array_map( 'array_unique', $fonts );

        return $fonts;
    }
    add_filter( 'pre_google_web_fonts', 'thelandscaper_get_customizer_fonts' );
}

/**
 * Adds custom classes to the array of body classes.
 */
if( ! function_exists( 'thelandscaper_body_class' ) ) {
    function thelandscaper_body_class( $class ) {

        $page_id = thelandscaper_get_correct_page_id();

        if ( is_multi_author() ) {
            $class[] = 'group-blog';
        }

        // If theme mod isset to boxed add body.boxed
        if ( 'boxed' === get_theme_mod( 'qt_theme_layout', 'wide' ) ) {
            $class[] = 'boxed';
        }

        // If theme mod isset to sticky navigation add body.sticky
        if ( 'sticky' === get_theme_mod( 'qt_nav_position', 'static' ) ) {
            $class[] = 'fixed-navigation';
        }

        // If doubletab is enabled add class to body
        if ( 'yes' === get_theme_mod( 'qt_nav_double_tap', 'yes' ) ) {
            $class[] = 'doubletap';
        }

        // If theme mod and ACF page option isset to hide topbar add body.no-topbar
        if ( 'hide' === get_theme_mod( 'qt_topbar', 'show' ) || 'hide' === get_field( 'topbar' ) ) {
            $class[] = 'no-topbar';
        }

        // If ACF page option isset to hide page header add body.no-page-header
        if ( 'header-hide' === get_field( 'page_title_area' ) ) {
            $class[] = 'no-page-header';
        }

        // Add the class for the sidebar position
        $get_sidebar = get_field( 'display_sidebar', $page_id );

        if ( is_single() && 'post' === get_post_type() ) {
            $get_sidebar = get_field( 'display_sidebar', get_option( 'page_for_posts' ) );
        }

        if ( thelandscaper_woocommerce_active() && is_woocommerce() ) {
            $get_sidebar = thelandscaper_single_product_sidebar();
        }

        if ( ! $get_sidebar ) { 
            $get_sidebar = 'hide';
        }

        if ( ! $page_id ) { 
            $get_sidebar = 'right';
        }

        // $get_sidebar = strtolower( $get_sidebar );
        $class[] = 'sidebar-'. esc_attr( strtolower( $get_sidebar ) );

        // If single gallery layout isset to split or fullwidth
        if ( 'split' === get_field( 'gallery_layout' ) ) {
            $class[] = 'gallery-layout-split';
        }

        // Get header layout from theme customizer
        $header = thelandscaper_get_header_layout();
        $class[] = 'header-'. $header;

        return $class;
    }
    add_filter( 'body_class', 'thelandscaper_body_class' );
}

/**
 * the_content read more link
 */
if ( ! function_exists( 'thelandscaper_read_more_link' ) ) {
    function thelandscaper_read_more_link() {

        $read_more = get_theme_mod( 'qt_blog_read_more', 'Read More' );

        if ( ! $read_more ) {
            $read_more = esc_html__( 'READ MORE', 'the-landscaper-wp' );
        }

        return '<a class="read more" href="'. esc_url( get_permalink() ) .'">'. esc_html( $read_more ) .'</a>';
    }
    add_filter( 'the_content_more_link', 'thelandscaper_read_more_link' );
}

/**
 * Add Custom styles to the Formats Dropdown in TinyMCE
 */
if ( ! function_exists( 'thelandscaper_tinymce_shortcodes' ) ) {
    function thelandscaper_tinymce_shortcodes( $settings ) {

        $headings = array(
            array(
                'title'   => esc_html__( 'Heading 1', 'the-landscaper-wp' ),
                'block'   => 'h1',
                'classes' => 'custom-title'
            ),
            array(
                'title'   => esc_html__( 'Heading 2', 'the-landscaper-wp' ),
                'block'   => 'h2',
                'classes' => 'custom-title'
            ),
            array(
                'title'   => esc_html__( 'Heading 3', 'the-landscaper-wp' ),
                'block'   => 'h3',
                'classes' => 'custom-title'
            ),
            array(
                'title'   => esc_html__( 'Heading 4', 'the-landscaper-wp' ),
                'block'   => 'h4',
                'classes' => 'custom-title'
            ),
            array(
                'title'   => esc_html__( 'Heading 5', 'the-landscaper-wp' ),
                'block'   => 'h5',
                'classes' => 'custom-title'
            ),
            array(
                'title'   => esc_html__( 'Heading 6', 'the-landscaper-wp' ),
                'block'   => 'h6',
                'classes' => 'custom-title'
            ),
        );

        $style_formats = array(
            array(
                'title'   => esc_html__( 'QT: Custom Headings', 'the-landscaper-wp' ),
                'items'   => $headings
            )
        );

    $settings['style_formats_merge'] = true;
    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

  }
  add_filter( 'tiny_mce_before_init', 'thelandscaper_tinymce_shortcodes' );
}

/**
 * Enable font size and custom heading buttons in the tiny mce editor
 */
if ( ! function_exists( 'thelandscaper_tinymce_more_buttons' ) ) {
    function thelandscaper_tinymce_more_buttons( $buttons ) {
        $buttons[3] = 'styleselect';
        $buttons[2] = 'fontsizeselect';
        
        return $buttons;
    }
    add_filter( 'mce_buttons_3', 'thelandscaper_tinymce_more_buttons');
}

/**
 * Creates the style from the array for the page headers
 */
if ( ! function_exists( 'thelandscaper_header_array' ) ) {
    function thelandscaper_header_array( $settings ) {
        $array_style = 'style="';
        
        foreach ( $settings as $key => $value ) {
            if ( $value ) {
            
                // If background isset add url()
                if ( 'background-image' === $key ) {
                    $array_style .= $key . ': url(\'' . esc_url( $value ) . '\'); ';
                } else {
                    $array_style .= $key . ': ' . esc_attr( $value ) . '; ';
                }
            }
        }
        $array_style .= '"';

        return $array_style;
    }
}

/**
 * Change names & slug to gallery from portfolio CPT
 */
if ( ! function_exists( 'thelandscaper_portfolio_cpt_change' ) ) {
    function thelandscaper_portfolio_cpt_change( array $args ) {
        $labels = array(
            'name'               => esc_html__( 'Galleries', 'the-landscaper-wp' ),
            'singular_name'      => esc_html__( 'Gallery', 'the-landscaper-wp' ),
            'add_new'            => esc_html__( 'Add New Gallery', 'the-landscaper-wp' ),
            'add_new_item'       => esc_html__( 'Add New Gallery', 'the-landscaper-wp' ),
            'edit_item'          => esc_html__( 'Edit Gallery', 'the-landscaper-wp' ),
            'new_item'           => esc_html__( 'Add New Gallery', 'the-landscaper-wp' ),
            'view_item'          => esc_html__( 'View Gallery', 'the-landscaper-wp' ),
            'search_items'       => esc_html__( 'Search Galleries', 'the-landscaper-wp' ),
            'not_found'          => esc_html__( 'No galleries found', 'the-landscaper-wp' ),
            'not_found_in_trash' => esc_html__( 'No galleries found in trash', 'the-landscaper-wp' ),
        );

        $args['labels'] = $labels;
        $args['rewrite'] = array( 'slug' => get_theme_mod( 'qt_gallery_slug', 'gallery' ) );
        $args['has_archive'] = false;
        $args['show_in_rest'] = true;

        return $args;
    }
    add_filter( 'portfolioposttype_args', 'thelandscaper_portfolio_cpt_change' );
}

/**
 * Change names & slug to gallery category from portfolio CPT
 */
if ( ! function_exists( 'thelandscaper_portfolio_category_args' ) ) {
    function thelandscaper_portfolio_category_args( array $args ) {
        $labels = array(
            'name'          => esc_html__( 'Gallery Categories', 'the-landscaper-wp' ),
            'singular_name' => get_theme_mod( 'qt_gallery_cat_title', 'Portfolio Category' ),
        );

        $args['labels'] = $labels;
        $args['rewrite'] = array( 'slug' => get_theme_mod( 'qt_gallery_cat_slug', 'portfolio_category' ) );
        $args['show_in_rest'] = true;

        return $args;
    }
    add_filter( 'portfolioposttype_category_args', 'thelandscaper_portfolio_category_args' );
}

/**
 * Change names & slug to gallery tag from portfolio CPT
 */
if ( ! function_exists( 'thelandscaper_portfolio_tag_args' ) ) {
    function thelandscaper_portfolio_tag_args( array $args ) {
        $labels = array(
            'name'          => esc_html__( 'Gallery Tags', 'the-landscaper-wp' ),
            'singular_name' => get_theme_mod( 'qt_gallery_cat_title', 'Portfolio Category' ),
        );

        $args['labels'] = $labels;

        return $args;
    }
    add_filter( 'portfolioposttype_tag_args', 'thelandscaper_portfolio_tag_args' );
}

/**
 * Add Skype protocol, skype:username?call can be used
 */
if ( ! function_exists( 'thelandscaper_skype_protocol' ) ) {
    function thelandscaper_skype_protocol( $protocols ){
        $protocols[] = 'skype';
        return $protocols;
    }
    add_filter( 'kses_allowed_protocols' , 'thelandscaper_skype_protocol' );
}

/**
 * Define the custom options for the SiteOrigin Page Builder
 */
if ( ! function_exists( 'thelandscaper_define_custom_pagebuilder_options' ) ) {
    function thelandscaper_define_custom_pagebuilder_options( $fields ) {

        $fields['white_widget_title'] = array(
            'name'          => esc_html__( 'White Widget Title', 'the-landscaper-wp' ),
            'label'         => esc_html__( 'Use white colored widget title', 'the-landscaper-wp' ),
            'type'          => 'checkbox',
            'group'         => 'design',
            'priority'      => 16,
        );

        $fields['text_center'] = array(
            'name'          => esc_html__( 'Text Center', 'the-landscaper-wp' ),
            'label'         => esc_html__( 'Center all text in widget', 'the-landscaper-wp' ),
            'type'          => 'checkbox',
            'group'         => 'design',
            'priority'      => 17,
        );

        $fields['border_box'] = array(
            'name'          => esc_html__( 'Border Widget', 'the-landscaper-wp' ),
            'label'         => esc_html__( 'Set a border around the widget', 'the-landscaper-wp' ),
            'type'          => 'checkbox',
            'group'         => 'design',
            'priority'      => 18,
        );

        $fields['content_box'] = array(
            'name'          => esc_html__( 'Border Widget + Title', 'the-landscaper-wp' ),
            'label'         => esc_html__( 'Set a border around the widget and use a slightly smaller title', 'the-landscaper-wp' ),
            'type'          => 'checkbox',
            'group'         => 'design',
            'priority'      => 19,
        );

        return $fields;
    }
    add_filter( 'siteorigin_panels_widget_style_fields', 'thelandscaper_define_custom_pagebuilder_options' );
}

/**
 * Add some custom option to the SiteOrigin Page Builder widget styles panel
 */
if ( ! function_exists( 'thelandscaper_add_custom_options_to_pagebuilder' ) ) {
    function thelandscaper_add_custom_options_to_pagebuilder( $attributes, $args ) {

        if ( ! empty( $args['white_widget_title'] ) ) {
            array_push( $attributes['class'], 'white' );
        }

        if ( ! empty( $args['text_center'] ) ) {
            array_push( $attributes['class'], 'text-center' );
        }

        if ( ! empty( $args['border_box'] ) ) {
            array_push( $attributes['class'], 'border-box' );
        }

        if ( ! empty( $args['content_box'] ) ) {
            array_push( $attributes['class'], 'content-box' );
        }

        return $attributes;
    }
    add_filter( 'siteorigin_panels_widget_style_attributes', 'thelandscaper_add_custom_options_to_pagebuilder', 10, 2 );
}

/**
 * Hide the live editor button in the admin bar by default
 */
if ( ! function_exists( 'thelandscaper_hide_live_editor_adminbar' ) ) {
    function thelandscaper_hide_live_editor_adminbar( $defaults ) {
        $defaults['live-editor-quick-link'] = false;
        return $defaults;
    }
    add_filter( 'siteorigin_panels_settings_defaults', 'thelandscaper_hide_live_editor_adminbar' );
}

/**
 * Remove the SiteOrigin premium teaser
 */
add_filter( 'siteorigin_premium_upgrade_teaser', '__return_false' );

/**
 * Define our prebuilt templates to the page builder layouts modal
 */
if ( ! function_exists( 'thelandscaper_prebuilt_page_layouts' ) ) {
    function thelandscaper_prebuilt_page_layouts( $layouts ) {

        $layouts['default-home'] = array_merge(
            array(
                'name'          => esc_html__( 'Default Home', 'the-landscaper-wp' ),
                'description'   => esc_html__( 'Slider need to be configured manually, please select the full width slider template and scroll below the page builder grid', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/homepage.png' ),
            ),
            json_decode( '{"widgets":[{"type":"html","title":"Exceptional Services We Offer","text":"We provide exceptional landscaping services to a wide range of commercial and residential properties for over 35 years, including large corporate environments, city parks, shopping malls and appartments. Our experienced landscapers set the standard each day in landscape design, paving, hardscaping. We will whip your yard into shape in no time.\n\n<a class=\"more\" href=\"http://export-xml.qreativethemes.com/thelandscaper/services//about-us/\">SEE ALL SERVICES</a>","filter":"1","id":"black-studio-tinymce-714210000","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":0,"id":0,"widget_id":"83588dc1-013d-400b-a11b-d46f134dd95f","style":{"class":"border-box","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"page_id":673,"btn_text":"READ MORE","id":"qt_feature_page-714210001","option_name":"widget_qt_feature_page","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":0,"cell":1,"id":1,"widget_id":"630620aa-2e65-40f7-ab5d-38d109e36419","style":{"background_image_attachment":false,"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"page_id":"670","btn_text":"READ MORE","id":"qt_feature_page-714210002","option_name":"widget_qt_feature_page","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":0,"cell":2,"id":2,"widget_id":"52cbb9d7-5337-4822-aca6-4971827610e7","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"page_id":667,"btn_text":"READ MORE","id":"qt_feature_page-714210003","option_name":"widget_qt_feature_page","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":0,"cell":3,"id":3,"widget_id":"f4eda9d9-203b-4072-ae4f-00dbbd76a36e","style":{"background_image_attachment":false,"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"Project Galleries","text":"<p>With so many years of experience in the business, our company is your<br /> source for the highest quality and landscaping service.</p>","filter":"1","id":"black-studio-tinymce-714210004","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":1,"cell":0,"id":4,"widget_id":"26100f2f-6be6-4dc2-91d1-0803888d52cd","style":{"class":"text-center no-border white","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"ess_grid_title":"","ess_grid":"4","ess_grid_pages":"","id":"ess-grid-widget-714210005","option_name":"widget_ess-grid-widget","panels_info":{"class":"Essential_Grids_Widget","raw":false,"grid":1,"cell":0,"id":5,"widget_id":"1a92083b-a350-4cf9-ba54-1e594b82effe","style":{"background_image_attachment":false,"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"About Our Company","text":"<p>The Landscaper is a full-service landscaping company with a straightforward and unique design/build philosophy. We believe in having one landscape designer handle the job from its conception on paper, to the realization on your property. The reason; by doing this you are able to communicate and work with a single individual, where you can share your thoughts and idea\'s with to bring them, in collaboration, to life.</p><p>The Landscaper is made up of a group of highly skilled landscaping professionals who pays a lot of attention to small details. In the 30+ years of experience our staff keep your property looking and functioning beautifully. Our landscapers are fully licensed</p><p>The reason; by doing this you are able to communicate and work with a single individual, where you can share your thoughts and idea\'s with to bring them, in collaboration...</p><p><a class=\"more\" href=\"#\">MORE ABOUT US</a></p>","filter":"1","id":"black-studio-tinymce-714210006","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":2,"cell":0,"id":6,"widget_id":"5ec2c99a-f913-44d2-86b5-5549ff2e7400","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"Request An Estimate","text":"<p>[contact-form-7 id=\"3275\" title=\"Request Quote\"]</p>","filter":"0","id":"black-studio-tinymce-714210007","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":2,"cell":1,"id":7,"widget_id":"175d1432-71fc-4477-8dc3-45d3faff1df7","style":{"class":"content-box text-center","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Why Choose Us?","text":"","filter":false,"id":"text-714210008","option_name":"widget_text","panels_info":{"class":"WP_Widget_Text","raw":false,"grid":3,"cell":0,"id":8,"widget_id":"b803c0b6-7ce7-44a9-a35a-c635851b6776","style":{"class":"no-border","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Specialized Company","text":"We are a landscaping company specialize in residential and commercial landscaping","link":"#","icon":"fa-leaf","layout":"small","id":"qt_icon_box-714210009","option_name":"widget_qt_icon_box","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":4,"cell":0,"id":9,"widget_id":"68431039-1c43-476b-9111-890c3ea01347","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Day Scheduling","text":"We schedule regular appointments to visit your property on the same day and time of the week","link":"","icon":"fa-calendar-o","layout":"small","id":"qt_icon_box-714210010","option_name":"widget_qt_icon_box","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":4,"cell":0,"id":10,"widget_id":"5d733997-1e68-4be4-b924-247c493d05f6","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Licensed &amp; Insured","text":"All our landscapers are fully licensed, bonded and insured for their safety","link":"","icon":"fa-check-circle-o","layout":"small","id":"qt_icon_box-714210011","option_name":"widget_qt_icon_box","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":4,"cell":1,"id":11,"widget_id":"c73fb78f-ff15-45c1-9c7a-2b171857da0c","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Free Consultations","text":"We offer free consultations for our services, and will provide you with an actual quote","link":"","icon":"fa-comments-o","layout":"small","id":"qt_icon_box-714210012","option_name":"widget_qt_icon_box","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":4,"cell":1,"id":12,"widget_id":"e145b8a5-0745-4cf7-bdbe-4f874dcdac26","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Dependable Services","text":"We love to take pride in the work we do. Each project is finished in time and budget","link":"","icon":"fa-tachometer","layout":"small","id":"qt_icon_box-714210013","option_name":"widget_qt_icon_box","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":4,"cell":2,"id":13,"widget_id":"ed26768f-b15f-4521-b01c-73b5ffdb82b7","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Reputable Company","text":"Operating for more than 30 years, earning a reputation for service and beautiful work","link":"","icon":"fa-trophy","layout":"small","id":"qt_icon_box-714210014","option_name":"widget_qt_icon_box","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":4,"cell":2,"id":14,"widget_id":"d5c11d72-a67c-45bd-94af-60d58d61443d","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Are you tired of spending hours mowing your lawn every weekend? <br>Take back your free time by having us take care of your garden","title_color":"","subtitle":"","subtitle_color":"","buttons":"[button href=\"#\" style=\"outline\"]See Our Services[/button] [button href=\"#\" style=\"outline\"]Get in Touch[/button]","layout":"cta-block","id":"qt_cta_banner-714210015","option_name":"widget_qt_cta_banner","panels_info":{"class":"QT_CTA_Banner","raw":false,"grid":5,"cell":0,"id":15,"widget_id":"d9778505-8c0e-455d-bf40-a0c274c63784","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Latest News","categories":"-1","count":3,"columns":3,"sticky":"on","page_id":18,"id":"qt_recent_posts_block-714210016","option_name":"widget_qt_recent_posts_block","panels_info":{"class":"QT_Recent_Posts_Block","raw":false,"grid":6,"cell":0,"id":16,"widget_id":"f1e020eb-a28e-48da-a7cf-5291fd8fb338","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"header":"Client Testimonials","columns":"2","autocycle":"yes","interval":6000,"touch_support":"yes","testimonials":{"3":{"id":"3","quote":"The Landscaper is a very good landscaping company. They do tree removal also. We liked them because all the employees are hard working, honest and reliable. They always come on time, and the prices are good too!","author":"Ross N. Haynes","location":"New York, US"},"4":{"id":"4","quote":"What a great job carried out by The Landscaper. From the initial planning to completion. Always on time, and left the site spotless on completion. We definitely going to use your landscaping services again in the future!","author":"David & Jennifer","location":"Orlando, US"},"5":{"id":"5","quote":"We found The Landscaper a pleasure to work with. The staff was friendly, hard working and completed the projects within the time agreed upon. Our own ideas were very carefully listened to and reflected in the design","author":"Bob & Kate","location":"London, UK"},"6":{"id":"6","quote":"I wanted to take this opportunity to thank you for the excellent service your company provides. Our yard always looks perfect when you are done. You are very thorough and always go the extra mile. Thanks again!","author":"Peter","location":"Barcelona, Spain"}},"id":"qt_testimonials-714210017","option_name":"widget_qt_testimonials","panels_info":{"class":"QT_Testimonials","raw":false,"grid":7,"cell":0,"id":17,"widget_id":"c6563c99-5433-47aa-a9d1-59b45d251ec9","style":{"background_image_attachment":false,"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"Landscaping Awards","text":"<p>With so many years of experience in the business, our company is your<br /> source for the highest quality and landscaping service.</p><div class=\"client-logos\"><div class=\"row\"><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client1.jpg\" alt=\"Client 1\" width=\"195\" height=\"123\" data-toggle=\"tooltip\" data-original-title=\"Award Nr. 1\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client2.jpg\" alt=\"Client 2\" width=\"195\" height=\"123\" data-toggle=\"tooltip\" data-original-title=\"Award Nr. 2\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client3.jpg\" alt=\"Client 3\" width=\"195\" height=\"123\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client4.jpg\" alt=\"Client 4\" width=\"195\" height=\"123\" data-toggle=\"tooltip\" data-original-title=\"Award Nr. 4\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client5.jpg\" alt=\"Client 5\" width=\"195\" height=\"123\" data-toggle=\"tooltip\" data-original-title=\"Award Nr. 5\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client6.jpg\" alt=\"Client 6\" width=\"195\" height=\"123\" /></div></div></div>","filter":"0","id":"black-studio-tinymce-714210018","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":8,"cell":0,"id":18,"widget_id":"7cccd769-d878-4533-a8ae-cea05db47dd0","style":{"class":"text-center no-border","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"lat":"37.787103","lng":"-77.110664","title":"<strong>The Company Location</strong><br><em>200 Park Avenue, New York<br>+123 - 777 - 456 - 789</em>","zoom":8,"type":"roadmap","style":"Paper","pin":"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/09/map_marker_02.png","height":400,"mobile_drag":"","id":"qt_google_map-714210019","option_name":"widget_qt_google_map","panels_info":{"class":"QT_Google_Map","raw":false,"grid":9,"cell":0,"id":19,"widget_id":"d54f51e3-f748-4abe-9657-3817a6694a8b","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}}],"grids":[{"cells":4,"style":{"background_display":"tile","bottom_margin":"55px","row_stretch":"full"}},{"cells":1,"style":{"padding":"55px","background_image_attachment":"6595","background_image_attachment_fallback":false,"background_display":"tile","bottom_margin":"60px","row_stretch":"full"}},{"cells":2,"style":{"background_display":"tile","bottom_margin":"45px"}},{"cells":1,"style":{"background_display":"tile","bottom_margin":"15px"}},{"cells":3,"style":{"background_display":"tile","bottom_margin":"70px"}},{"cells":1,"style":{"padding":"40px","background_image_attachment":"6247","background_image_attachment_fallback":false,"background_display":"cover","bottom_margin":"0px","row_stretch":"full"}},{"cells":1,"style":{"padding":"60px","background_image_attachment":"5256","background_image_attachment_fallback":false,"background_display":"tile","bottom_margin":"0px","row_stretch":"full"}},{"cells":1,"style":{"padding":"75px","background_image_attachment":"4143","background_image_attachment_fallback":false,"background_display":"tile","border_color":"#e6e6e6","bottom_margin":"60px","row_stretch":"full"}},{"cells":1,"style":{"background_display":"tile","bottom_margin":"70px"}},{"cells":1,"style":{"background_display":"tile","bottom_margin":"0px","row_stretch":"full-stretched"}}],"grid_cells":[{"grid":0,"index":0,"weight":0.333428571429,"style":[]},{"grid":0,"index":1,"weight":0.222,"style":[]},{"grid":0,"index":2,"weight":0.222285714286,"style":[]},{"grid":0,"index":3,"weight":0.222285714286,"style":[]},{"grid":1,"index":0,"weight":1,"style":[]},{"grid":2,"index":0,"weight":0.5,"style":[]},{"grid":2,"index":1,"weight":0.5,"style":[]},{"grid":3,"index":0,"weight":1,"style":[]},{"grid":4,"index":0,"weight":0.333333333333,"style":[]},{"grid":4,"index":1,"weight":0.333333333333,"style":[]},{"grid":4,"index":2,"weight":0.333333333333,"style":[]},{"grid":5,"index":0,"weight":1,"style":[]},{"grid":6,"index":0,"weight":1,"style":[]},{"grid":7,"index":0,"weight":1,"style":[]},{"grid":8,"index":0,"weight":1,"style":[]},{"grid":9,"index":0,"weight":1,"style":[]}]}', true )
        );

        $layouts['about-us'] = array_merge(
            array(
                'name'          => esc_html__( 'About Us', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/about-us.png' ),
            ),
            json_decode( '{"widgets":[{"type":"visual","title":"We Are The Landscaper","text":"<p>The Landscaper is a full-service landscaping company with a straightforward and unique design/build philosophy. We believe in having one landscape designer handle the job from its conception on paper, to the realization on your property. The reason; by doing this you are able to communicate and work with a single individual, where you can share your thoughts and idea\'s with to bring them, in collaboration, to life.</p><p>The Landscaper is made up of a group of highly skilled landscaping professionals who pays a lot of attention to small details. In the 30+ years of experience our staff keep your property looking and functioning beautifully. Plus our landscapers are fully licensed.</p><p><a class=\"more\" href=\"#\">Get in Touch</a></p>","filter":"1","id":"black-studio-tinymce-18110000","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":0,"id":0,"widget_id":"346c9995-693a-42b3-ab60-e675b3e5a8b8","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"html","title":"","text":"[twentytwenty]\n<a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\"><img class=\"alignnone size-full wp-image-6235\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\" alt=\"\" width=\"850\" height=\"450\" /></a>\n<a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\"><img class=\"alignnone size-full wp-image-6235\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\" alt=\"\" width=\"850\" height=\"450\" /></a>\n[/twentytwenty]","filter":"0","id":"black-studio-tinymce-18110001","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":1,"id":1,"widget_id":"7006e573-ea7a-4531-8a77-30cc97addfc0","style":{"background_image_attachment":false,"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"","text":"[dropcap style=\"style1\" title=\"Started the Company\"]1978[/dropcap]Almost four decades ago we started as a company with two man. Not much later a landscape designer joined the team and from that point we began to grow fast.","filter":false,"id":"text-18110002","option_name":"widget_text","panels_info":{"class":"WP_Widget_Text","raw":false,"grid":1,"cell":0,"id":2,"widget_id":"c0b2cf00-2386-42f7-a013-9317617669ef","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"","text":"[dropcap style=\"style1\" title=\"Second Location\"]1997[/dropcap]The last years we grew very quickly, so 1997 was the year we opened a second location. The new location started with five new landscapers.","filter":false,"id":"text-18110003","option_name":"widget_text","panels_info":{"class":"WP_Widget_Text","raw":false,"grid":1,"cell":1,"id":3,"widget_id":"bab7347e-c9c2-40ce-a7d5-a4b8af1252c9","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"","text":"[dropcap style=\"style1\" title=\"Total of 12 Awards\"]2015[/dropcap]Until the year 2015, we are nominated several times, and won 12 awards in total for landscape design. We are very proud of this achievement.","filter":false,"id":"text-18110004","option_name":"widget_text","panels_info":{"class":"WP_Widget_Text","raw":false,"grid":1,"cell":2,"id":4,"widget_id":"862c49d3-d376-4d89-84b1-13e22bfac79e","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"The Landscapers","text":"","filter":false,"id":"text-18110005","option_name":"widget_text","panels_info":{"class":"WP_Widget_Text","raw":false,"grid":2,"cell":0,"id":5,"widget_id":"a2ed7483-8774-4f16-bba5-fe83853af19a","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\"><img class=\"alignnone size-full wp-image-6237\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\" alt=\"placeholder_360x240\" width=\"360\" height=\"240\" /></a></p><h4>John Williams</h4><p><span style=\"color: #a2c046; font-size: 15px;\"><strong>OWNER</strong></span></p><p>Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Omnia contraria, quos etiam insanos esse vultis. Diodorus, eius auditor, adiungit ad honestatem vacuitatem</p>","filter":"0","id":"black-studio-tinymce-18110006","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":3,"cell":0,"id":6,"widget_id":"e95edeb5-53bf-41ed-a5ff-98dfc86ef00c","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\"><img class=\"alignnone size-full wp-image-6237\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\" alt=\"placeholder_360x240\" width=\"360\" height=\"240\" /></a></p><h4>Larry Vieira</h4><p><span style=\"color: #a2c046; font-size: 15px;\"><strong>SENIOR LANDSCAPER</strong></span></p><p>Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Omnia contraria, quos etiam insanos esse vultis. Diodorus, eius auditor, adiungit ad honestatem vacuitatem</p>","filter":"0","id":"black-studio-tinymce-18110007","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":3,"cell":1,"id":7,"widget_id":"0a676a5f-71a2-485a-bb5b-4dcc7175c940","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\"><img class=\"alignnone size-full wp-image-6237\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\" alt=\"placeholder_360x240\" width=\"360\" height=\"240\" /></a></p><h4>Dwight Rogers</h4><p><span style=\"color: #a2c046; font-size: 15px;\"><strong>LANDSCAPER</strong></span></p><p>Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Omnia contraria, quos etiam insanos esse vultis. Diodorus, eius auditor, adiungit ad honestatem vacuitatem</p>","filter":"1","id":"black-studio-tinymce-18110008","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":3,"cell":2,"id":8,"widget_id":"8c83b61a-e5be-44bd-a864-422725372059","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\"><img class=\"alignnone size-full wp-image-6237\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\" alt=\"placeholder_360x240\" width=\"360\" height=\"240\" /></a></p><h4>Ralph Hayes</h4><p><span style=\"color: #a2c046; font-size: 15px;\"><strong>SENIOR LANDSCAPER</strong></span></p><p>Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Omnia contraria, quos etiam insanos esse vultis. Diodorus, eius auditor, adiungit ad honestatem vacuitatem</p>","filter":"0","id":"black-studio-tinymce-18110009","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":4,"cell":0,"id":9,"widget_id":"5b8922b6-72bc-4578-932d-6dac89a125e7","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\"><img class=\"alignnone size-full wp-image-6237\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\" alt=\"placeholder_360x240\" width=\"360\" height=\"240\" /></a></p><h4>Frank Anderson</h4><p><span style=\"color: #a2c046; font-size: 15px;\"><strong>LANDSCAPER</strong></span></p><p>Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Omnia contraria, quos etiam insanos esse vultis. Diodorus, eius auditor, adiungit ad honestatem vacuitatem</p>","filter":"0","id":"black-studio-tinymce-18110010","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":4,"cell":1,"id":10,"widget_id":"0f1c38cf-79d8-401d-883f-a94b3c990ef7","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"html","title":"We are hiring Landscapers","text":"Are you a top producer who is looking to show your landscaping skills to us and be fairly rewarded? Are you able to manage job sites and read landscape drawings?\n<h6>&nbsp;</h6>\n<b>At The Landscaper we offer:</b>\n<strong><span style=\"font-size: 16px; color: #a2c046;\">[fa icon=\"fa-check-circle-o\"]</span></strong>  Full Time, Year Round employment\n<strong><span style=\"font-size: 16px; color: #a2c046;\">[fa icon=\"fa-check-circle-o\"]</span></strong>  Shared benefit package\n<strong><span style=\"font-size: 16px; color: #a2c046;\">[fa icon=\"fa-check-circle-o\"]</span></strong>  Excellent work environment\n\n<a class=\"more\" href=\"#\">READ FULL JOB DESCRIPTION</a>","filter":"1","id":"black-studio-tinymce-18110011","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":4,"cell":2,"id":11,"widget_id":"bae2d708-abd7-4f64-ad82-8d1b5a51710f","style":{"class":"border-box","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"count_before":"","count_to":870,"count_after":"+","count_icon":"fa-check-circle-o","title":"Gardens Designed","id":"qt_count_box-18110012","option_name":"widget_qt_count_box","panels_info":{"class":"QT_Count_Box","raw":false,"grid":5,"cell":0,"id":12,"widget_id":"2a6719ae-9d91-477e-ad8d-a44a4249ad4d","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"count_before":"","count_to":110,"count_after":"%","count_icon":"fa-comments","title":"Satisfied Clients","id":"qt_count_box-18110013","option_name":"widget_qt_count_box","panels_info":{"class":"QT_Count_Box","raw":false,"grid":5,"cell":1,"id":13,"widget_id":"0275b571-c2f3-4ed6-82c3-0b575f9054a1","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"count_before":"","count_to":4820,"count_after":" m2","count_icon":"fa-leaf","title":"Turf Laid","id":"qt_count_box-18110014","option_name":"widget_qt_count_box","panels_info":{"class":"QT_Count_Box","raw":false,"grid":5,"cell":2,"id":14,"widget_id":"d0a59e8b-2506-4701-a269-7ca513fcdf2a","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"count_before":"","count_to":25,"count_after":"+","count_icon":"fa-users","title":"Landscapers","id":"qt_count_box-18110015","option_name":"widget_qt_count_box","panels_info":{"class":"QT_Count_Box","raw":false,"grid":5,"cell":3,"id":15,"widget_id":"113e5556-eb33-4e9f-8c72-b451a645ced3","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"Landscaping Awards","text":"<p>With so many years of experience in the business, our company is your<br /> source for the highest quality and landscaping service.</p><div class=\"client-logos\"><div class=\"row\"><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client1.jpg\" alt=\"Client 1\" width=\"195\" height=\"123\" data-toggle=\"tooltip\" data-original-title=\"Award Nr. 1\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client2.jpg\" alt=\"Client 2\" width=\"195\" height=\"123\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client3.jpg\" alt=\"Client 3\" width=\"195\" height=\"123\" data-toggle=\"tooltip\" data-original-title=\"Award Nr. 3\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client4.jpg\" alt=\"Client 4\" width=\"195\" height=\"123\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client5.jpg\" alt=\"Client 5\" width=\"195\" height=\"123\" /></div><div class=\"col-xs-12 col-sm-4 col-md-2\"><img src=\"http://tf-images.qreativethemes.com/thelandscaper/client6.jpg\" alt=\"Client 6\" width=\"195\" height=\"123\" data-toggle=\"tooltip\" data-original-title=\"Award Nr. 6\" /></div></div></div>","filter":"1","id":"black-studio-tinymce-18110016","option_name":"widget_black-studio-tinymce","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":6,"cell":0,"id":16,"widget_id":"de446f05-9882-4734-aaf5-fb5ff449d2ba","style":{"class":"text-center no-border","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}}],"grids":[{"cells":2,"style":{"background_display":"tile","bottom_margin":"50px"}},{"cells":3,"style":{"background_display":"tile","bottom_margin":"60px","gutter":"25px"}},{"cells":1,"style":{"background_display":"tile","bottom_margin":"0px"}},{"cells":3,"style":{"background_display":"tile","bottom_margin":"50px"}},{"cells":3,"style":{"background_display":"tile","bottom_margin":"70px"}},{"cells":4,"style":{"padding":"55px","background_image_attachment":"4143","background_image_attachment_fallback":false,"background_display":"tile","bottom_margin":"60px","gutter":"19px","row_stretch":"full"}},{"cells":1,"style":{"background_display":"tile","cell_alignment":"flex-start"}}],"grid_cells":[{"grid":0,"index":0,"weight":0.5,"style":[]},{"grid":0,"index":1,"weight":0.5,"style":[]},{"grid":1,"index":0,"weight":0.333333333333,"style":[]},{"grid":1,"index":1,"weight":0.333333333333,"style":[]},{"grid":1,"index":2,"weight":0.333333333333,"style":[]},{"grid":2,"index":0,"weight":1,"style":[]},{"grid":3,"index":0,"weight":0.333333333333,"style":[]},{"grid":3,"index":1,"weight":0.333333333333,"style":[]},{"grid":3,"index":2,"weight":0.333333333333,"style":[]},{"grid":4,"index":0,"weight":0.333333333333,"style":[]},{"grid":4,"index":1,"weight":0.333333333333,"style":[]},{"grid":4,"index":2,"weight":0.333333333333,"style":[]},{"grid":5,"index":0,"weight":0.25,"style":[]},{"grid":5,"index":1,"weight":0.25,"style":[]},{"grid":5,"index":2,"weight":0.25,"style":[]},{"grid":5,"index":3,"weight":0.25,"style":[]},{"grid":6,"index":0,"weight":1,"style":[]}],"name":"About Us"}', true )
        );

        $layouts['services'] = array_merge(
            array(
                'name'          => esc_html__( 'Services', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/services.png' ),
            ),
            json_decode( '{"widgets":[{"page_id":"673","btn_text":"READ MORE","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":0,"cell":0,"id":0,"widget_id":"328196bb-4c7d-422d-9b99-71c7e49e4e7d","style":{"background_display":"tile"}}},{"page_id":"664","btn_text":"READ MORE","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":0,"cell":1,"id":1,"widget_id":"ab904c19-c43a-4985-95f3-dc4f88ee1f34","style":{"background_display":"tile"}}},{"page_id":"670","btn_text":"READ MORE","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":0,"cell":2,"id":2,"widget_id":"280b1029-3f6b-41c2-b4ea-8926a7c16adc","style":{"background_display":"tile"}}},{"page_id":"661","btn_text":"READ MORE","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":1,"cell":0,"id":3,"widget_id":"2af8348f-9128-4691-b00b-c8057d1bba75","style":{"background_display":"tile"}}},{"page_id":"676","btn_text":"READ MORE","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":1,"cell":1,"id":4,"widget_id":"27b6ac97-c04f-4195-abf3-06686dc98a86","style":{"background_display":"tile"}}},{"page_id":"667","btn_text":"READ MORE","panels_info":{"class":"QT_Feature_Page","raw":false,"grid":1,"cell":2,"id":5,"widget_id":"10f48f23-a41c-4d9a-8859-737baff8d88f","style":{"background_display":"tile"}}},{"title":"Tired of spending your free time mowing grass?","title_color":"","subtitle":"","subtitle_color":"","buttons":"[button href=\"gallery\" style=\"outline\"]See Our Gallery[/button] [button href=\"make-an-appointment\" style=\"outline\"]Make An Appointment[/button]","layout":"cta-inline","panels_info":{"class":"QT_CTA_Banner","raw":false,"grid":2,"cell":0,"id":6,"widget_id":"85a0d541-a0ed-480b-9290-041b8174b139","style":{"background_image_attachment":false,"background_display":"tile"}}}],"grids":[{"cells":3,"style":{"bottom_margin":"50px","background_display":"tile"}},{"cells":3,"style":{"bottom_margin":"70px","background_image_attachment":false,"background_display":"tile"}},{"cells":1,"style":{"row_css":"margin: 0 0 -60px 0;","row_stretch":"full","background_image_attachment":6247,"background_display":"cover"}}],"grid_cells":[{"grid":0,"index":0,"weight":0.333333333333,"style":{}},{"grid":0,"index":1,"weight":0.333333333333,"style":{}},{"grid":0,"index":2,"weight":0.333333333333,"style":{}},{"grid":1,"index":0,"weight":0.333333333333,"style":{}},{"grid":1,"index":1,"weight":0.333333333333,"style":{}},{"grid":1,"index":2,"weight":0.333333333333,"style":{}},{"grid":2,"index":0,"weight":1,"style":{}}],"name":"Services"}', true )
        );

        $layouts['single-services'] = array_merge(
            array(
                'name'          => esc_html__( 'Single Services', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/single-service.png' ),
            ),
            json_decode( '{"widgets":[{"type":"visual","title":"","text":"<p>We understand that your property is a big investment and we want our customers to know that we are there for them to help them accomplish the dreams that they may have for there property. From new construction landscaping to existing landscapes that need a facelift, our professional staff helps you. Makes the landscape experience just that, an experience</p><p><span style=\"color: #a2c046; font-size: 15px;\"><b>BEAUTIFULLY LAWN AND FLAWLESS GARDENING</b></span></p><p>Our landscape division has many years of experience in all phases of landscaping and our customer focused approach makes the landscape experience just that, an experience. We want our customers to be there from the moment we get to the job until the end of the job so that they can have as much input.</p><p><strong><span style=\"color: #a2c046; font-size: 15px;\">ATTENTION TO THE SMALLEST DETAILS</span></strong></p><p>We understand that your property is a big investment and we want our customers to know that we are there for them to help them accomplish the dreams that they may have for there property. From new construction landscaping to existing landscapes that need a facelift, our professional staff helps you. We want our customers to be there from the moment</p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":0,"id":0,"widget_id":"d00ccdbc-33e6-46d8-9eab-d8417fbc384f","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\"><img class=\"alignnone size-full wp-image-6235\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\" alt=\"\" width=\"850\" height=\"450\" /></a></p><p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\"><img class=\"alignnone size-full wp-image-6235\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\" alt=\"\" width=\"850\" height=\"450\" /></a></p>","filter":"0","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":1,"id":1,"widget_id":"d8331453-ec03-48d9-ac63-7996ca5deb1f","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p><img class=\"aligncenter wp-image-6403 size-full\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/07/img_divider.png\" alt=\"img_divider\" width=\"1128\" height=\"14\" /></p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":1,"cell":0,"id":2,"widget_id":"2ea0279a-abe0-4c6d-8738-dedd6e9e5633","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<h3>Key Benefits of the Service</h3><p>Watering your lawn and is the key to preserving its lushness and beauty. The experts carefully design your sprinkler system to ensure maximum coverage to your lawn. Whether you are looking for a brand new irrigation system to maintain your lawn.</p><p><span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Extend your home with a beautiful garden<br /> <span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Your property value increases many times initial value<br /> <span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Aesthetic garden beauty that improves with age<br /> <span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Allround certified and insured landscapers<br /> <span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Our Landscapers attend annual training seminars</p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":2,"cell":0,"id":3,"widget_id":"8f434dec-7b1f-4ed4-9e25-2c8f5a1f1c7e","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"","text":"[collapsibles]\n    [collapse title=\"Our Qualifications\" state=\"active\"]Our landscape division has many years of experience in all phases of landscaping and our customer focused approach makes us the best landscaping company!\n    [/collapse]\n    [collapse title=\"Our Capabilities\"]Our landscape division has many years of experience in all phases of landscaping and our customer focused approach makes us the best landscaping company!\n    [/collapse]\n    [collapse title=\"Our Commitment\"]Our landscape division has many years of experience in all phases of landscaping and our customer focused approach makes us the best landscaping company!\n    [/collapse]\n[/collapsibles]","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":2,"cell":1,"id":4,"widget_id":"fe781f30-f91d-463b-8aea-5e3bc13a1570","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Interested In This Service? Make An Appointment","title_color":"","subtitle":"","subtitle_color":"","buttons":"[button style=\"outline\" href=\"#\"]Make Appointment[/button]","layout":"cta-inline","panels_info":{"class":"QT_CTA_Banner","raw":false,"grid":3,"cell":0,"id":5,"widget_id":"c9343208-e052-4d04-b362-6a35f7085332","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}}],"grids":[{"cells":2,"style":{"background_display":"tile"}},{"cells":1,"style":{"background_display":"tile","bottom_margin":"40px"}},{"cells":2,"style":{"background_display":"tile","bottom_margin":"50px"}},{"cells":1,"style":{"background_image_attachment":"6247","background_display":"cover"}}],"grid_cells":[{"grid":0,"index":0,"weight":0.5,"style":[]},{"grid":0,"index":1,"weight":0.5,"style":[]},{"grid":1,"index":0,"weight":1,"style":[]},{"grid":2,"index":0,"weight":0.5,"style":[]},{"grid":2,"index":1,"weight":0.5,"style":[]},{"grid":3,"index":0,"weight":1,"style":[]}],"name":"Single Service"}', true )
        );

        $layouts['single-services-two'] = array_merge(
            array(
                'name'          => esc_html__( 'Single Services 2', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/single-service-2.png' ),
            ),
            json_decode( '{"widgets":[{"type":"visual","title":"","text":"<p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\"><img class=\"alignnone size-full wp-image-6235\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\" alt=\"\" width=\"850\" height=\"450\" /></a></p><p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\"><img class=\"alignnone size-full wp-image-6235\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_850x450.png\" alt=\"\" width=\"850\" height=\"450\" /></a></p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":0,"id":0,"widget_id":"db7a7331-d6f1-4ebf-afce-8362f965015c","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p>Our fall clean-up service is the process of removing all leaves, branches and other debris that have accumulated over the course of the summer season.   It is important for the health of your landscaping to remove these material because they harbor fungal material such as spores that harm the health of plants in your landscaping.</p><p><span style=\"color: #a2c046; font-size: 15px;\"><b>GIVE YOUR PROPERTY A HEALTHY START</b></span></p><p>Our landscape division has many years of experience in all phases of landscaping and our customer focused approach makes the landscape experience just that, an experience. We want our customers to be there from the moment we get to the job until the end of the job so that they can have as much input.</p><p><strong><span style=\"color: #a2c046; font-size: 15px;\">MAKE SURE YOUR LAWNS ARE PROPERLY PREPARED</span></strong></p><p>We understand that your property is a big investment and we want our customers to know that we are there for them to help them accomplish the dreams that they may have for there property. From new construction landscaping to existing landscapes that need a facelift, our professional staff helps you. We want our customers to be there from the moment.</p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":1,"id":1,"widget_id":"152a86a0-db52-4971-b6d9-80d1443a4296","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p><img class=\"size-full wp-image-6403 aligncenter\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/07/img_divider.png\" alt=\"img_divider\" width=\"1128\" height=\"14\" /></p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":1,"cell":0,"id":2,"widget_id":"224f6bc2-a671-4314-9cb1-fb64f66a57de","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Request a Call Back","text":"Fill in the request form and we\'ll contact you by phone shortly","link":"#","icon":"fa-phone","layout":"big","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":2,"cell":0,"id":3,"widget_id":"1e416c07-4ef9-4600-bac4-dd6f058d317e","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Make An Appointment","text":"To schedule an appointment and discuss your project with us","link":"#","icon":"fa-calendar-o","layout":"big","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":2,"cell":0,"id":4,"widget_id":"3d019312-5043-40e0-a946-42d0870fca83","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<h3>Key Benefits of the Service</h3><p>Watering your lawn and is the key to preserving its lushness and beauty. The experts carefully design your sprinkler system to ensure maximum coverage to your lawn. Whether you are looking for a brand new irrigation system to maintain your lawn.</p><p><span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Extend your home with a beautiful garden<br /><span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Your property value increases many times initial value<br /><span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Aesthetic garden beauty that improves with age<br /><span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Allround certified and insured landscapers<br /><span style=\"color: #a2c046; font-size: 18px;\">[fa icon=\"fa-check-circle-o\"]</span>  Our Landscapers attend annual training seminars</p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":2,"cell":1,"id":5,"widget_id":"3d4b0b5b-149f-4794-a1b0-fc9f4c2ecc91","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}}],"grids":[{"cells":2,"style":{"background_display":"tile"}},{"cells":1,"style":{"bottom_margin":"40px","background_display":"tile"}},{"cells":2,"style":[]}],"grid_cells":[{"grid":0,"index":0,"weight":0.5,"style":{}},{"grid":0,"index":1,"weight":0.5,"style":{}},{"grid":1,"index":0,"weight":1,"style":{}},{"grid":2,"index":0,"weight":0.5,"style":{}},{"grid":2,"index":1,"weight":0.5,"style":{}}],"name":"Single Service 2"}', true )
        );

        $layouts['prices-delivery'] = array_merge(
            array(
                'name'          => esc_html__( 'Prices & Delivery', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/prices-delivery.png' ),
            ),
            json_decode( '{"widgets":[{"title":"","text":"Feel free to ask any landscaping or gardening questions over the phone, or get in touch via our contact form below. Your message will be dispatched directly to our staff who will answer as soon as they can.","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":0,"cell":0,"id":0,"widget_id":"98b014dc-a0e2-4967-8059-535d5ab984b7","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"","text":"[table cols=\"Subdivision/Town, Zip Code, Dump Truck, Flatbed\" data=\"\nAbbott Glen, 40014, $50.00, $60.00,\nAcademy Ridge, 40245, $45.00, $55.00,\nAikenshire, 40245, $50.00, $60.00,\nAnchorage (Lucas Rd off Lucas), 40223, $35.00, $45.00,\nAnchorage (Osage and beyond), 40223, $35.00, $45.00,\nAnchorage Pointe\t, 40223, $35.00, $45.00,\nAnchorage Woods, 40223, $35.00, $45.00,\nArbor Ridge, 40014, $45.00, $55.00\"]","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":0,"cell":0,"id":1,"widget_id":"0f2e2f02-6f4f-4b70-bf95-1880d52a27c4","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<h2 class=\"custom-title\">Delivery Conditions</h2>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":1,"cell":0,"id":2,"widget_id":"add4e554-17d5-45ef-9f00-f77b7c26cd91","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"","brochure_icon":"fa-file-pdf-o","brochure_url":"#","brochure_btn":"Download Delivery Charges PDF","new_tab":"","panels_info":{"class":"QT_Brochure","raw":false,"grid":2,"cell":0,"id":3,"widget_id":"8b29b735-61c8-4bb8-9ddf-346e585b8a07","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"","brochure_icon":"fa-file-word-o","brochure_url":"#","brochure_btn":"Download Delivery Charges DOC","new_tab":"","panels_info":{"class":"QT_Brochure","raw":false,"grid":2,"cell":1,"id":4,"widget_id":"95bf3950-c793-4ab4-814e-80dc69d9fcfc","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}}],"grids":[{"cells":1,"style":{"background_display":"tile"}},{"cells":1,"style":{"background_display":"tile","bottom_margin":"0px"}},{"cells":2,"style":[]}],"grid_cells":[{"grid":0,"index":0,"weight":1,"style":[]},{"grid":1,"index":0,"weight":1,"style":[]},{"grid":2,"index":0,"weight":0.5,"style":[]},{"grid":2,"index":1,"weight":0.5,"style":[]}],"name":"Prices & Delivery"}', true )
        );

        $layouts['frequently-asked-questions'] = array_merge(
            array(
                'name'          => esc_html__( 'Frequently Asked Questions', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/frequently-asked-questions.png' ),
            ),
            json_decode( '{"widgets":[{"title":"","text":"[dropcap style=\"style3\" title=\"Who will work on my property?\"]Q[/dropcap]Well trained, supervised personnel with the proper equipment. All employees wear a company uniform.","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":0,"cell":0,"id":0,"widget_id":"4e729ba4-fb97-492e-9f09-4035b629c494","style":{"background_image_attachment":false,"background_display":"tile"}}},{"title":"","text":"[dropcap style=\"style3\" title=\"Are your workers Certified?\"]Q[/dropcap]We have the highest percentage of certified landscapers in the country. We also have a certified snow staff.","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":0,"cell":0,"id":1,"widget_id":"f9f4c141-f4a3-4cc7-861e-95a7d39f2a89","style":{"background_image_attachment":false,"background_display":"tile"}}},{"title":"","text":"[dropcap style=\"style3\" title=\"Are you a licensed Landscaper?\"]Q[/dropcap]Yes, we are fully licensed. We hold all the required and necessary licenses for a landscaping company.","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":0,"cell":1,"id":2,"widget_id":"88bbe900-1197-4b1f-b46a-ee8306b0f104","style":{"background_image_attachment":false,"background_display":"tile"}}},{"title":"","text":"[dropcap style=\"style3\" title=\"What clients do you work for?\"]Q[/dropcap]Anyone who truly wants a nice landscape and knows the true value of caring for a beautiful home and property.","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":0,"cell":1,"id":3,"widget_id":"795f001d-a4b2-4c1e-9dbe-0bbb9403b662","style":{"background_image_attachment":false,"background_display":"tile"}}},{"title":"","text":"[dropcap style=\"style3\" title=\"Who will work on my property?\"]Q[/dropcap]Well trained, supervised personnel with the proper equipment. All employees wear a company uniform.","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":0,"cell":2,"id":4,"widget_id":"a1402c73-d0b3-498d-a97b-1b5e2bcaf955","style":{"background_image_attachment":false,"background_display":"tile"}}},{"title":"","text":"[dropcap style=\"style3\" title=\"Are your workers Certified?\"]Q[/dropcap]We have the highest percentage of certified landscapers in the country. We also have a certified snow professional staff.","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":0,"cell":2,"id":5,"widget_id":"47edbe38-3dce-4936-acdc-b8f478466c7a","style":{"background_image_attachment":false,"background_display":"tile"}}},{"title":"Accordion Style","text":"[collapsibles]\n    [collapse title=\"Who will work on my property?\" state=\"active\"]\n        Well trained, supervised personnel with the proper equipment. All employees wear a company uniform. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quoniam, si dis placet, ab Epicuro loqui discimus. Maximus dolor, inquit, brevis est. Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.\n    [/collapse]\n    [collapse title=\"Are you a licensed Landscaper?\"]\n       Yes, we are fully licensed. We hold all the required and necessary licenses for a landscaping company. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quoniam, si dis placet, ab Epicuro loqui discimus. Maximus dolor, inquit, brevis est. Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.\n    [/collapse]\n    [collapse title=\"Are your workers Certified?\"]\n        We have the highest percentage of certified landscapers in the country. We also have a certified snow professional staff. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quoniam, si dis placet, ab Epicuro loqui discimus. Maximus dolor, inquit, brevis est. Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.\n    [/collapse]\n    [collapse title=\"How do you schedule your clients?\"]\n        We try to schedule our business clients early in the week and our residential clients are scheduled Tuesday through Friday. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quoniam, si dis placet, ab Epicuro loqui discimus. Maximus dolor, inquit, brevis est. Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.\n    [/collapse]\n    [collapse title=\"What type of clients do you work for?\"]\n        Anyone who truly wants a nice landscape and knows the true value of caring for a beautiful home and property. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quoniam, si dis placet, ab Epicuro loqui discimus. Maximus dolor, inquit, brevis est. Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.\n    [/collapse]\n[/collapsibles]","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":1,"cell":0,"id":6,"widget_id":"cbc92cd3-d0f0-4de9-a5b7-f8a105c34651","style":{"background_display":"tile"}}},{"type":"visual","title":"Ask Us A Question","text":"<p>Feel free to ask any landscaping or gardening questions through the<br />contact form below. We will answer as soon as we can.</p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":2,"cell":0,"id":7,"widget_id":"ecde3c07-a35b-4988-b820-3a4e4ba9728b","style":{"class":"text-center no-border","background_display":"tile"}}},{"title":"","text":"[contact-form-7 id=\"4\" title=\"Ask Us A Question\"]","filter":false,"panels_info":{"class":"WP_Widget_Text","raw":false,"grid":2,"cell":0,"id":8,"widget_id":"bff3be9b-d6b5-405a-b7b7-4f18178c964f","style":{"background_display":"tile"}}}],"grids":[{"cells":3,"style":{"bottom_margin":"50px","background_image_attachment":false,"background_display":"tile"}},{"cells":1,"style":{"bottom_margin":"70px","background_display":"tile"}},{"cells":1,"style":{"row_css":"margin-bottom: -60px;","padding":"50px","row_stretch":"full","background_image_attachment":4143,"background_display":"tile"}}],"grid_cells":[{"grid":0,"index":0,"weight":0.333333333333,"style":{}},{"grid":0,"index":1,"weight":0.333333333333,"style":{}},{"grid":0,"index":2,"weight":0.333333333333,"style":{}},{"grid":1,"index":0,"weight":1,"style":{}},{"grid":2,"index":0,"weight":1,"style":{}}],"name":"Frequently Asked Questions"}', true )
        );

        $layouts['contact-us'] = array_merge(
            array(
                'name'          => esc_html__( 'Contact Us', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/contact-us.png' ),
            ),
            json_decode( '{"widgets":[{"type":"visual","title":"Send Us A Message","text":"<p style=\"margin-bottom: 20px;\">Feel free to ask any landscaping or gardening questions over the phone, or get in touch via our contact form below. Your message will be dispatched directly to our staff who will answer as soon as they can.</p><p>[contact-form-7 id=\"630\" title=\"Contact Form\"]</p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":true,"grid":0,"cell":0,"id":0,"widget_id":"41e73bda-9df6-4844-9553-589a37ae4f51","style":{"id":"","class":"","widget_css":"","mobile_css":"","margin":"","padding":"","mobile_padding":"","background":"","background_image_attachment":"0","background_image_attachment_fallback":"","background_display":"tile","border_color":"","font_color":"","link_color":""}}},{"type":"html","title":"Contact Details","text":"<strong>The Landscaper, Ltd.</strong>\n\n200 Park Avenue,\nNew York, NY, US\n\nTelephone: +(212) 777 - 123 - 456\nE-mail: <a href=\"mailto:info@companyname.com\">info@yourdomain.com</a>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":1,"id":1,"widget_id":"3e6b82a4-1bf4-43df-9ab5-c5743865742d","style":{"class":"content-box","background_image_attachment":false,"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Working Hours","Monday_from":"07:00","Monday_to":"17:00","Tuesday_from":"07:00","Tuesday_to":"17:00","Wednesday_from":"07:00","Wednesday_to":"17:00","Thursday_from":"07:00","Thursday_to":"17:00","Friday_from":"07:00","Friday_to":"17:00","Saturday_from":"07:00","Saturday_to":"17:00","Sunday_from":"07:00","Sunday_to":"","separator":"-","closed":"WE ARE CLOSED","highlight":"on","panels_info":{"class":"QT_Opening_Hours","raw":false,"grid":0,"cell":1,"id":2,"widget_id":"56d41259-c7a0-4b2f-af64-ddb31077ac15","style":{"class":"content-box","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"lat":"40.524981","lng":"-74.245054","title":"<strong>The Company Location</strong><br><em>202 Park Avenue, New York<br>+123 - 777 - 456 - 789</em>","zoom":14,"type":"roadmap","style":"Paper","pin":"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/09/map_marker_02.png","height":400,"mobile_drag":"","panels_info":{"class":"QT_Google_Map","raw":false,"grid":1,"cell":0,"id":3,"widget_id":"14a359a1-5157-4b46-af0b-fff4d2eb890c","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}}],"grids":[{"cells":2,"style":{"background_display":"tile","bottom_margin":"60px"}},{"cells":1,"style":{"row_css":"margin-bottom: -60px;","background_display":"tile","row_stretch":"full-stretched"}}],"grid_cells":[{"grid":0,"index":0,"weight":0.700188458253,"style":[]},{"grid":0,"index":1,"weight":0.299811541747,"style":[]},{"grid":1,"index":0,"weight":1,"style":[]}],"name":"Contact Us"}', true )
        );

        $layouts['make-an-appointment'] = array_merge(
            array(
                'name'          => esc_html__( 'Make an Appointment', 'the-landscaper-wp' ),
                'screenshot'    => get_theme_file_uri( '/assets/images/pre-built/make-an-appointment.png' ),
            ),
            json_decode( '{"widgets":[{"type":"visual","title":"Have a Question?","text":"<p><a href=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\"><img class=\"alignnone size-full wp-image-6237\" src=\"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/01/placeholder_360x240.png\" alt=\"placeholder_360x240\" width=\"360\" height=\"240\" /></a></p><p>Feel free to ask any landscaping or gardening questions over the phone.</p><h3><span style=\"color: #9fc612;\"><span style=\"color: #ababab;\">[fa icon=\"fa-phone\"] </span> 123 777 456 789</span></h3>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":0,"id":0,"widget_id":"5c921175-7c57-4926-88df-2e80f09096c0","style":{"class":"content-box","background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Contact Details","text":"200 Park Avenue, New York, US  +123 - 777 - 456 - 789","link":"","icon":"fa-home","layout":"big","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":0,"cell":0,"id":1,"widget_id":"7882e025-5d98-4f27-bfe5-048f274c6892","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"title":"Working Hours","text":"Monday - Friday: 08:00 - 17:00 Saturday &amp; Sunday: CLOSED","link":"","icon":"fa-clock-o","layout":"big","panels_info":{"class":"QT_Icon_Box","raw":false,"grid":0,"cell":0,"id":2,"widget_id":"d1b1e86a-5dc5-4673-9f33-7c25ad890d22","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"type":"visual","title":"","text":"<p>Feel free to ask any landscaping or gardening questions over the phone, or get in touch via our contact form below. Your message will be dispatched directly to our staff who will answer as soon as they can.</p><h6> </h6><p>[contact-form-7 id=\"5267\" title=\"Schedule An Appointment\"]</p>","filter":"1","panels_info":{"class":"WP_Widget_Black_Studio_TinyMCE","raw":false,"grid":0,"cell":1,"id":3,"widget_id":"260e82eb-c941-42fe-ba3a-5fba4f6b0ff8","style":{"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}},{"lat":"37.787103","lng":"-77.110664","title":"<strong>The Shop Location</strong><br><em>200 Park Avenue, New York<br>+123 - 777 - 456 - 789</em>","zoom":8,"type":"roadmap","style":"Paper","pin":"http://export-xml.qreativethemes.com/thelandscaper/wp-content/uploads/sites/2/2015/09/map_marker_02.png","height":400,"mobile_drag":"","panels_info":{"class":"QT_Google_Map","raw":false,"grid":1,"cell":0,"id":4,"widget_id":"9212ef33-559b-4058-a5c3-57f35f657f02","style":{"background_image_attachment":false,"background_display":"tile","white_widget_title":"","text_center":"","border_box":"","content_box":""}}}],"grids":[{"cells":2,"style":{"bottom_margin":"50px","gutter":"40px","background_display":"tile"}},{"cells":1,"style":{"row_css":"margin-bottom: -60px;","row_stretch":"full-stretched","background_display":"tile"}}],"grid_cells":[{"grid":0,"index":0,"weight":0.333460269104,"style":{}},{"grid":0,"index":1,"weight":0.666539730896,"style":{}},{"grid":1,"index":0,"weight":1,"style":{}}],"name":"Make an Appointment"}', true )
        );

        return $layouts;
    }
    add_filter( 'siteorigin_panels_prebuilt_layouts', 'thelandscaper_prebuilt_page_layouts' );
}