<?php
/**
 * Get all the custom widgets for the Theme
 *
 * @package The Landscaper
 */

/* Icon Box Widget */
require get_theme_file_path( '/inc/widgets/widget-icon-box.php' );

/* Opening Hours Widget */
require get_theme_file_path( '/inc/widgets/widget-opening-hours.php' );

/* Featured Page Widget */
require get_theme_file_path( '/inc/widgets/widget-featured-page.php' );

/* Social Icons Widget */
require get_theme_file_path( '/inc/widgets/widget-social-icons.php' );

/* Testimonials Widget */
require get_theme_file_path( '/inc/widgets/widget-testimonials.php' );

/* Call To Action Banner Widget */
require get_theme_file_path( '/inc/widgets/widget-cta-banner.php' );

/* Call To Action Button Widget */
require get_theme_file_path( '/inc/widgets/widget-cta-button.php' );

/* Counter Box Widget */
require get_theme_file_path( '/inc/widgets/widget-count-box.php' );

/* Google Map Widget */
require get_theme_file_path( '/inc/widgets/widget-google-map.php' );

/* Recent Post List Widget */
require get_theme_file_path( '/inc/widgets/widget-recent-posts-list.php' );

/* Recent Post Block Widget */
require get_theme_file_path( '/inc/widgets/widget-recent-posts-block.php' );

/* Brochure Widget */
require get_theme_file_path( '/inc/widgets/widget-brochure.php' );

/* Facebook Page Box Widget */
require get_theme_file_path( '/inc/widgets/widget-facebook.php' );

/* Team Member Widget */
require get_theme_file_path( '/inc/widgets/widget-team-member.php' );

/* Register all widgets */
function thelandscaper_register_widget() {

	// Define all theme widgets
	$thelandscaper_widget_names = array(
		'QT_Brochure',
		'QT_Count_Box',
		'QT_CTA_Banner',
		'QT_CTA_Button',
		'QT_Facebook',
		'QT_Feature_Page',
		'QT_Google_Map',
		'QT_Icon_Box',
		'QT_Opening_Hours',
		'QT_Recent_Posts_Block',
		'QT_Recent_Posts',
		'QT_Social_Icons',
		'QT_Team_Member',
		'QT_Testimonials'
	);

	foreach ( $thelandscaper_widget_names as $widget_names ) {
		register_widget( $widget_names );
	}
}
add_action( 'widgets_init', 'thelandscaper_register_widget' );