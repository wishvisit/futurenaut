<?php
/**
 * One click demo import functions
 *
 * @package the-landscaper
 */

/**
 *  Define all files that need to be imported
 */
function thelandscaper_import_files() {
    return array(
        array(
            'import_file_name'             => 'The Landscaper',
            'local_import_file'            => get_theme_file_path( '/demo-files/content.xml' ),
            'local_import_widget_file'     => get_theme_file_path( '/demo-files/widgets.json' ),
            'local_import_customizer_file' => get_theme_file_path( '/demo-files/customizer.dat' ),
            'import_preview_image_url'     => get_theme_file_uri( 'screenshot.png' ),
            'import_notice'                => sprintf( esc_html__( 'Please use the demo importer only on a clean installation. Use the %1s plugin to clean the installation (this will delete all content)', 'the-landscaper-wp' ), '<a href="https://wordpress.org/plugins/wp-reset/" target="blank">WordPress Reset</a>' ),
            'preview_url'                  => 'https://demos.qreativethemes.com/thelandscaper',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'thelandscaper_import_files' );

/**
 *  Filters
 */
add_filter( 'pt-ocdi/regenerate_thumbnails_in_content_import', '__return_false' );
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

/**
 *  After import setup
 */
function thelandscaper_after_import_setup() {

    // Menus to Import and assign - you can remove or add as many as you want
    $footer_menu   = get_term_by( 'name', 'Footer Menu', 'nav_menu' );
	$main_menu	   = get_term_by( 'name', 'Primary Navigation', 'nav_menu' );
	$services_menu = get_term_by( 'name', 'Services Menu', 'nav_menu' );

	set_theme_mod( 'nav_menu_locations', array(
		'primary' => $main_menu->term_id,
		'services-menu' => $services_menu->term_id,
		'footer-menu' => $footer_menu->term_id
	) );

	// Set the front page and blog page
	$set_front_page = get_page_by_title( 'Front Page' )->ID;
	$set_blog_page  = get_page_by_title( 'Blog' )->ID;

	update_option( 'show_on_front', 'page' );
	update_option( 'page_on_front', $set_front_page );
	update_option( 'page_for_posts', $set_blog_page );

	// Empty default breadcrumbs seperator
	add_option( 'bcn_options', array( 'hseparator' => '' ) );

	// Force the logo in the customizer on import
	set_theme_mod( 'qt_logo', get_theme_file_uri( '/assets/images/logo.png' ) );
	set_theme_mod( 'qt_logo_transparent', get_theme_file_uri( '/assets/images/logo_transparent.png' ) );

	// Force 404 image in the customizer on import
	set_theme_mod( 'qt_404_page_image', get_theme_file_uri( '/assets/images/404_image.png' ) );

	// Force bottom footer text in the customizer on import
	set_theme_mod( 'qt_footerbottom_textleft', 'Copyright 2018 The Landscaper by Qreativethemes' );
	set_theme_mod( 'qt_footerbottom_textmiddle', 'Edit: Appearance > Customize > Theme Options > Footer' );
	set_theme_mod( 'qt_footerbottom_textright', 'For emergency tree removal 123-777-456' );
}
add_action( 'pt-ocdi/after_import', 'thelandscaper_after_import_setup' );