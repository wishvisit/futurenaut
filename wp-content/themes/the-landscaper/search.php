<?php
/**
 * The template for displaying search results pages.
 *
 * @package The Landscaper
 */

get_header();

// Get the Main Title Template Part
get_template_part( 'parts/main-title' );

// Get the sidebar
$sidebar = get_field( 'display_sidebar', (int) get_option( 'page_for_posts' ) );
if ( ! $sidebar ) :
	$sidebar = 'right';
endif;
?>

<section class="content">
	<div class="container">
		<div class="row">
			<main class="col-xs-12<?php echo ( is_active_sidebar( 'sidebar' ) && 'Left' === $sidebar ) ? ' col-md-9 col-md-push-3' : ''; echo ( is_active_sidebar( 'sidebar' ) && 'Right' === $sidebar ) ? ' col-md-9' : ''; ?>">
				
				<ul class="search-list-results">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<li>
							<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						</li>
					<?php endwhile; else : ?>
						<h2><?php esc_html_e( 'Sorry, no results found.', 'the-landscaper-wp'); ?></h2>
					<?php endif; ?>
				</ul>
				
			</main>

			<?php if ( 'Hide' !== $sidebar && is_active_sidebar( 'sidebar' ) ) : ?>
				<div class="col-xs-12 col-md-3">
					<aside class="sidebar">
						<?php dynamic_sidebar( 'sidebar' ); ?>
					</aside>
				</div>
			<?php endif; ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>