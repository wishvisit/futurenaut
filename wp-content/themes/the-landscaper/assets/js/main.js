// Require JS Configurations
require.config({

	paths: {
		jquery: 'assets/js/return.jquery',
		underscore: 'assets/js/return.underscore'
	}
});

// Get the base url of the teme
require.config( {
	baseUrl: TheLandscaper.themePath
});

require([
	'jquery',
	'underscore',
	'assets/js/maps',
	'assets/js/stickynav',
	'assets/js/scrollToTop',
	'assets/js/doubletaptogo',
	'assets/js/jquery-mobile-touch-support.min',
], function ( $, _ ) {

	// Properly update the ARIA states on blur (keyboard) and mouse out events
	$( '[role="menubar"]' ).on( 'focus.aria  mouseenter.aria', '[aria-haspopup="true"]', function ( ev ) {
		$( ev.currentTarget ).attr( 'aria-expanded', true );
	} );

	$( '[role="menubar"]' ).on( 'blur.aria  mouseleave.aria', '[aria-haspopup="true"]', function ( ev ) {
		$( ev.currentTarget ).attr( 'aria-expanded', false );
	} );

	// If website visited on touch device fix for display submenu
	if( $( 'body' ).hasClass( 'doubletap' ) ) {
		$( '.menu-item-has-children' ).doubleTapToGo();
	}

	// Add toggle button for sub menu's on mobile view
	$( '.main-navigation li.menu-item-has-children' ).each( function() {
		$( this ).prepend( '<div class="nav-toggle-mobile-submenu"><i class="fa fa-caret-down"></i></div>' );
	});
	$( '.nav-toggle-mobile-submenu' ).click( function () {
		$( this ).parent().toggleClass( 'nav-toggle-dropdown' );
	} );

	// Bootstrap Tooltip used for awards hover
	$( '[data-toggle="tooltip"]' ).tooltip();

	// Add child page arrow to max mega menu parent links
	$( '#mega-menu-wrap-primary li.mega-menu-item-has-children > a' ).prepend( $( '<i class="fa fa-caret-down"></i>' ) );

	// Wrap mega menu around default header div
	$( '#mega-menu-wrap-primary' ).wrap( $( '<div class="main-navigation"></div>' ) );

	// Scroll to #href link (one-page option)
	$( '.main-navigation a[href^="#"]' ).on( 'click', function( event ) {
	    var target = $( this.getAttribute( 'href' ) );
	    if( target.length ) {
	        event.preventDefault();
	        $( 'html, body' ).stop().animate({
	            scrollTop: target.offset().top
	        }, 1000);
	    }
	});

	// Bootstrap accordion - active panel
	$( '.panel-title a' ).on( 'click', function(e) {
        e.preventDefault();
        if(!$(this).parents( '.panel-title' ).hasClass( 'active' ) ) {
            $( '.panel-title' ).removeClass( 'active' );
            $(this).parent().addClass( 'active' ).next().addClass( 'active' );
        } else {
            $( '.panel-title' ).removeClass( 'active' );
        }
    });

    // Bootstrap accordion - open by url hash
	$( document ).ready( function() {

		var pageurl = location.hash,
			stickynav = $( 'body' ).hasClass( 'fixed-navigation' ) ? $( '.navigation' ).outerHeight() + 110 : 110;

	    if( pageurl.length ) {
	        var accordion = $('a[href="' + pageurl + '"]');
	        $( 'html,body' ).animate({
	            scrollTop: accordion.parents( '.panel' ).offset().top - stickynav
	        }, 500, function() {
	            accordion.trigger( 'click' );
	        });
	    }
	});

	// Bootstrap carousel - jumbotron functions
	if( $( '.jumbotron' ).length ) {

		var $this = $( this ),
			sliderTouch = $( '.carousel-touch' );

		// Touch support
		sliderTouch.swiperight( function() {
			$( this ).carousel( 'prev' );
		});
		
		sliderTouch.swipeleft( function() {
			$( this ).carousel( 'next' );
		});
	}

	// Bootstrap carousel - testimonial functions
	if( $( '.testimonial-carousel' ).length ) {

		var $this = $( this ),
			sliderTouch = $( '.carousel-touch' );

		sliderTouch.swiperight( function() {
			$( this ).carousel( 'prev' );
		});
		
		sliderTouch.swipeleft( function() {
			$( this ).carousel( 'next' );
		});
	}

	// Wrap qt table in html for responsive view
	if( $( '.qt-table' ).length > 0 ) {
		$( '.qt-table' ).wrap( "<div class='qt-table-wrap'></div>" );
	}
});