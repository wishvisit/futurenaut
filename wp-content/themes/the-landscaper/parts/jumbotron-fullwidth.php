<?php
/**
 * Frontpage Slider Template Part
 *
 * @package The Landscaper
 */

// Carousel touch support option
$carousel_touch = get_field( 'enable_touch_support' );
if ( $carousel_touch ) { 
    $carousel_touch = 'carousel-touch';
}

$carousel_pause_hover = get_field( 'pause_hover' );
if ( $carousel_pause_hover ) { 
    $carousel_pause_hover = 'hover';
} else {
    $carousel_pause_hover = 'null';
}

// Carousel slider animation
$carousel_animation = get_field( 'slide_animation' );
?>

<div class="jumbotron carousel slide <?php echo esc_attr( $carousel_touch ); ?> <?php echo esc_attr( $carousel_animation ); ?>" id="jumbotron-fullwidth" data-ride="carousel" <?php printf( 'data-interval="%s"', get_field( 'auto_cycle' ) ? get_field( 'slide_interval' ) : 'false' ); ?> data-pause="<?php echo esc_attr( $carousel_pause_hover ); ?>">
    <div class="carousel-inner">

        <?php 
        // If only one slide is uploaded hide the controls
        $thelandscaper_count_slides = count( get_field( 'slide' ) );

        if ( $thelandscaper_count_slides > 1 ) : ?>
            <a class="carousel-control left" href="#jumbotron-fullwidth" role="button" data-slide="prev"><i class="fa fa-caret-left"></i></a>
            <a class="carousel-control right" href="#jumbotron-fullwidth" role="button" data-slide="next"><i class="fa fa-caret-right"></i></a>
        <?php endif; ?>
        
        <?php 
            $i = -1;
            while ( have_rows( 'slide' ) ) : 
                the_row();
                $i++;

                // Get the image
                $thelandscaper_get_slide_image = get_sub_field( 'slidebg_image' );

                // Get the image meta
                $thelandscaper_get_slide_image_alt = get_post_meta( $thelandscaper_get_slide_image, '_wp_attachment_image_alt', true );

                // Check if image alt text is added, else display slide heading as alt
                if ( $thelandscaper_get_slide_image_alt == '' ) {
                    $thelandscaper_get_slide_image_alt = get_sub_field( 'slide_heading' );
                }

                // Get the url for the img src
                $thelandscaper_slide_image = wp_get_attachment_image_src( $thelandscaper_get_slide_image, 'thelandscaper-home-slider-m' );

                // Get the srcset code
                $thelandscaper_slide_image_srcset = thelandscaper_srcset_sizes( $thelandscaper_get_slide_image, array( 'thelandscaper-home-slider-s', 'thelandscaper-home-slider-m', 'thelandscaper-home-slider-l' ) );

                // Get the caption option field
                $slide_caption = get_field( 'slide_captions' );

                // Get the link url field
                $link_slide = get_sub_field( 'link_slide' );

                // Get the link target field
                $link_target = get_sub_field( 'link_target' );
            ?>

            <div class="item <?php echo 0 === $i ? 'active' : ''; ?>">
                <?php if ( ! empty( $link_slide ) && $slide_caption === false ) : ?>
                    <a href="<?php echo esc_url( $link_slide ); ?>"<?php echo ( 'yes' === $link_target ) ? ' target="_blank"' : ''; ?>>
                <?php endif; ?>
                <img src="<?php echo esc_url( $thelandscaper_slide_image[0] ); ?>" width="<?php echo esc_attr( $thelandscaper_slide_image[1] ); ?>" height="<?php echo esc_attr( $thelandscaper_slide_image[2] ); ?>" srcset="<?php echo esc_html( $thelandscaper_slide_image_srcset ); ?>" sizes="100vw" alt="<?php echo esc_attr( strip_tags( $thelandscaper_get_slide_image_alt ) ); ?>">
               <?php if ( ! empty( $link_slide ) && $slide_caption === false ) : ?>
                    </a>
                <?php endif; ?>
                <?php if ( get_field( 'slide_captions' ) && ( get_sub_field( 'slide_top_heading' ) || get_sub_field( 'slide_heading' ) || get_sub_field( 'slide_text' ) ) ) : ?>
                    <div class="container">
                        <div class="carousel-text <?php echo esc_attr( get_field( 'caption_align' ) ); ?>">
                            <?php if( get_sub_field( 'slide_top_heading' ) ) : ?>
                                <div class="carousel-topheading"><?php the_sub_field( 'slide_top_heading' ); ?></div>
                            <?php endif; ?>
                            <?php if( get_sub_field( 'slide_heading' ) ) : ?>
                                <div class="carousel-heading"><h1><?php the_sub_field( 'slide_heading' ); ?></h1></div>
                                <?php endif; ?>
                            <?php if( get_sub_field( 'slide_text' ) || have_rows( 'slide_buttons' ) ) : ?>
                                <div class="carousel-content">
                                    <?php
                                        the_sub_field( 'slide_text' );
                                        while ( have_rows( 'slide_buttons' ) ) :
                                            the_row();
                                            // Get the slide button link
                                            $slide_button_link = get_sub_field( 'slide_button_link' );
                                            // Get the slide button text
                                            $slide_button_text = get_sub_field( 'slide_button_text' );
                                            // Get the slide button style
                                            $slide_button_style = get_sub_field( 'slide_button_style' );
                                            ?>
                                            <a href="<?php echo esc_url( $slide_button_link ); ?>" class="btn btn-<?php echo esc_attr( $slide_button_style ); ?>"><?php echo esc_html( $slide_button_text ); ?></a>
                                            <?php
                                        endwhile;
                                    ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        
        <?php endwhile; ?>

        <?php if( get_field( 'slide_indicators' ) === true ) : ?>
            <div class="container">
                <div class="col-xs-12">
                    <ol class="carousel-indicators">
                        <?php
                        $i = -1;
                        while ( have_rows( 'slide' ) ) : 
                            the_row();
                            $i++;
                        ?>
                            <li data-target="#jumbotron-fullwidth" data-slide-to="<?php echo esc_attr( $i ); ?>"<?php echo 0 === $i ? ' class="active"': ''; ?>></li>
                        <?php endwhile; ?>
                    </ol>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>