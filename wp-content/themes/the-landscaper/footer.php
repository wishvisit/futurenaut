<?php
/**
 * Footer Template Part
 *
 * @package The Landscaper
 */

// Set the footer on 4 columns
$main_footer_columns = (int)get_theme_mod( 'qt_footer_columns', 4 );

// Get the bottom footer text settings
$bottom_footer_left = get_theme_mod( 'qt_footerbottom_textleft' );
$bottom_footer_middle = get_theme_mod( 'qt_footerbottom_textmiddle' );
$bottom_footer_right = get_theme_mod( 'qt_footerbottom_textright' );
?>

<footer class="footer">

	<?php if ( $main_footer_columns > 0 && is_active_sidebar( 'main-footer' ) ) : ?>
		<div class="main-footer">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar( 'main-footer' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ( $bottom_footer_left != '' || $bottom_footer_middle != '' || $bottom_footer_right != '' ) : ?>
		<div class="bottom-footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<div class="bottom-left">
							<p><?php echo wp_kses_post( $bottom_footer_left ); ?></p>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="bottom-middle">
							<p><?php echo wp_kses_post( $bottom_footer_middle ); ?></p>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="bottom-right">
							<p><?php echo wp_kses_post( $bottom_footer_right ); ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ( 'hide' !== get_theme_mod( 'qt_scroll_to_top_button', 'show' ) ) : ?>
		<a class="scrollToTop" href="#">
			<i class="fa fa-angle-up"></i>
		</a>
	<?php endif; ?>

</footer>

</div><!-- end layout boxed wrapper -->

<?php wp_footer(); ?>
</body>
</html>