<div id="products">
    <div class="products-relative">
        <img class="hidden-xs hidden-sm" id="bg-product" src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-bg-products.png">
        <div class="container">
            <div class="products-content">
                <div class="row">
                    <div class="col-12">
                        <img id="product-head" src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-products.png">
                    </div>
                </div>
                <div class="row flexslider" id="product-list">
                    <ul class="slides">
                        <?php
                            $args = array(
                                'post_type'     => 'post',
                                'status'        => 'publish',
                                'category_name' => 'products',
                                'order'         => 'ASC',
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) :
                                while ( $query->have_posts() ) :
                                    $query->the_post();
                        ?>
                                
                                    <li>
                                        <div class="content">
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="col-12">
                                                    <?php the_post_thumbnail( 'medium_large' ); ?>
                                                </div>
                                                <div class="col-12 content-title">
                                                    <?php the_title(); ?>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                
                        <?php
                                endwhile; 
                            endif;
                        ?>
                    </ul>
                </div>
                <div class="read-more-product">
                    <a href="#" class="btn">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>