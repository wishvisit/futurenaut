<div id="contact">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6">
                <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-contact-us.png">
                <div class="contact-info">
                    <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-location.png">
                    <br>
                    <address>群馬県高崎市本町48</address>
                </div>
                <div class="contact-info">
                    <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-facebook.png">
                    <br>
                    <a href="https://www.facebook.com/Futurenaut.Inc"   target="_blank">Facebook</a><br>
                    <a href="https://www.twitter.com/FUTURENAUT_Inc"    target="_blank">Twitter</a><br>
                    <a href="https://www.instagram.com/futurenaut.inc"   target="_blank">Instagram</a>
                </div>
                <div class="contact-info">
                    <a href="mailto:info@futurenaut.co.jp">
                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-mail.png">
                        <br>                       
                        <p>info@futurenaut.co.jp</p>
                    </a>
                </div>
                <div class="contact-info">
                    <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/phone-26.png">
                    <br>
                    <p>TEL: 027-386-4607</p>
                    <p>FAX: 027-386-4608</p>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 form-contact">
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Your Email">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="message" rows="8" placeholder="Your Message"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success btn-block">SEND</button>
                </form>
            </div>
        </div>
        <img id="contact-btm" src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-contact-bottom.png">
    </div>
</div>