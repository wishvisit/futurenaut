<div id="about-us">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-7" id="text-about-us">
                <h3>About <span style="color:#ff7f24;">Futurenaut</span></h3>
                <br>
                <h4><span style="color:#ff7f24;">ミライの問題への挑戦・・・</span></h4>
                <p>世界では、現時点において8億人を超える人々が飢餓に直面しています。2050年には世界人口が90億人を超えると予測されており、未来の食料不足に対する懸念は高まる一方です。特に、動物性たんぱく質の需要拡大とともに、食肉の増産が求められますが、従来型の畜産業は環境負荷がとても大きく、持続可能性に大きな懸念が生じています。</p>


                <h4><span style="color:#ff7f24;">新しい食文化を創造する・・・</span></h4>
                <p>国連食糧農業機関（FAO）が発表した報告書『食用昆虫：食料と飼料の安全保障に向けた将来展望』を契機に、家畜に代わる新しい動物性たんぱく源として、昆虫の食用利用が注目されています。しかし、日本では昆虫食に対するゲテモノイメージが強く、世界が注目する「ミライの食」を受け入れる準備ができていないように思われます。</p>
                <p>新奇な食品に対する忌避心理をブレイクスルーするためには、安全で美味しい食べなれた食品の提案（昆虫食事業）と、ミライの食を受け入れられるようにする食育の推進（食育サービス事業）が必要です。私たちは、この2つの事業を車の両輪のように等しく駆動させることで、ミライの問題に真正面から向き合っていきます。</p>


                <h4><span style="color:#ff7f24;">大学発ベンチャーとして・・・</span></h4>
                <p>私たちは、『環境政策×地域づくり』を研究する大学の知的シーズを活用した“大学発ベンチャー企業”です。最新の学術的知見およびエビデンスに基づいたフードサービスを提供することで、群馬から日本の、世界の、そして未来の食料問題・環境問題の解決に貢献します。</p>

            </div>
            <div class="col-12 col-sm-12 col-md-5" id="logo-circle">
                <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/futurenaut-logo-with-leaf.png" alt="Futurenaut logo">
            </div>
        </div>
    </div>
    <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-about-bottom.png">
</div>