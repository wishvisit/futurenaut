<?php
/**
 * Futurenaut Header Layout
 *
 * @package The Landscaper 
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="layout-boxed">
    <header class="header">
        <div class="container">
            <div class="topbar panel hidden"></div>
            <div class="navigation" aria-label="Main Menu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="navbar-toggle-text"><?php esc_html_e( 'MENU', 'the-landscaper-wp' ); ?></span>
                        <span class="navbar-toggle-icon">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </span>
                    </button>

                    <a href="<?php echo get_home_url(); ?>" class="navbar-brand"><img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-logo.png"></a>
                </div>
                <nav id="navbar" class="collapse navbar-collapse">
                    <a href="<?php echo get_home_url(); ?>" class="navbar-brand hidden-xs hidden-sm"><img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-logo.png"></a>
                    <ul id="menu-primary-navigation" class="main-navigation" role="menubar">
                        <li><a href="<?php echo get_home_url(); ?>">HOME</a></li>
                        <li><a href="<?php echo get_home_url(); ?>#products">PRODUCTS</a></li>
                        <li><a href="<?php echo get_home_url(); ?>#news">NEWS</a></li>
                        <li><a href="<?php echo get_home_url(); ?>#contact">CONTACT</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>