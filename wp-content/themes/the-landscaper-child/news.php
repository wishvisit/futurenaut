<div id="news">
    <div class="news-relative">
        <img id="bg-news" src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-bg-news.png">
        <div class="container">
            <div class="news-content">
                <div class="row" id="news-head">
                    <h3><span style="color:#ffffff;">LATEST</span> <span style="color:#ff7f24;">NEWS</span></h3>
                </div>
                <div class="row flexslider" id="news-list">
                    <ul class="slides">
                        <?php
                            $args = array(
                                'post_type'     => 'post',
                                'status'        => 'publish',
                                'category_name' => 'news',
                                'order'         => 'ASC',
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) :
                                while ( $query->have_posts() ) :
                                    $query->the_post();
                        ?>
                                
                                    <li>
                                        <div class="content">
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="col-12">
                                                    <?php the_post_thumbnail( 'medium_large' ); ?>
                                                </div>
                                                <div class="col-12 content-title">
                                                    <?php the_title(); ?>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                
                        <?php
                                endwhile; 
                            endif;
                        ?>
                    </ul>
                </div>
                <div class="read-more-news">
                    <a href="#">Read more ></a>
                </div>
            </div>
        </div>
    </div>
</div>