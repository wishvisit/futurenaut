<?php
/*
* Template Name: Futurenaut
*/
?>
<?php get_header(); ?>
<?php include('carousel.php'); ?>
<?php include('about-us.php'); ?>
<?php include('products.php'); ?>
<?php include('news.php'); ?>
<?php include('contact-us.php'); ?>
<?php get_footer(); ?>