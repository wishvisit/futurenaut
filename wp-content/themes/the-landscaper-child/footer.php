<?php
/**
 * Footer Template Part
 *
 * @package The Landscaper
 */

// Set the footer on 4 columns
$main_footer_columns = (int)get_theme_mod( 'qt_footer_columns', 4 );

// Get the bottom footer text settings
$bottom_footer_left = get_theme_mod( 'qt_footerbottom_textleft' );
$bottom_footer_middle = get_theme_mod( 'qt_footerbottom_textmiddle' );
$bottom_footer_right = get_theme_mod( 'qt_footerbottom_textright' );
?>

<footer class="footer hidden-xs hidden-sm">
	<nav class="nav-footer">
		<div class="container">
			<ul role="menubar">
				<li><a href="<?php echo get_home_url(); ?>">HOME</a></li>
				<li><a href="<?php echo get_home_url(); ?>#about-us">ABOUT US</a></li>
				<li><a href="<?php echo get_home_url(); ?>#products">PRODUCTS</a></li>
				<li><a href="<?php echo get_home_url(); ?>#news">NEWS</a></li>
				<li><a href="<?php echo get_home_url(); ?>#contact">CONTACT</a></li>
			</ul>
		</div>
	</nav>
	<div class="logo-footer">
		<img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-logo.png">
	</div>
</footer>

</div><!-- end layout boxed wrapper -->
<script>
    jQuery(document).ready(function(){
        jQuery('#product-list.flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            touch: true,
            itemWidth: 285,
            itemMargin: 15
        });
		jQuery('#news-list.flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            touch: true,
            itemWidth: 285,
            itemMargin: 15
        });
    });
</script>
<?php wp_footer(); ?>
</body>
</html>