<?php

function parent_theme_enqueue_styles() {
    wp_enqueue_style( 'thelandscaper-main', get_template_directory_uri() . '/style.css', array( 'font-awesome', 'bootstrap' ) );
    wp_enqueue_style( 'thelandscaper-child-style', get_stylesheet_directory_uri() . '/style.css', 'thelandscaper-main' );
    wp_enqueue_style( 'futurenaut-style', get_theme_file_uri( '/assets/css/futurenaut.css' ), true);
    wp_enqueue_style( 'flexslider-style', get_theme_file_uri( '/assets/css/flexslider.css' ), true);
    wp_enqueue_script( 'flexslider-script', get_theme_file_uri( '/assets/js/jquery.flexslider.js' ), true);
}
add_action( 'wp_enqueue_scripts', 'parent_theme_enqueue_styles' );