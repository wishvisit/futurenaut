<div id="slides" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#slides" data-slide-to="0" class="active"></li>
        <li data-target="#slides" data-slide-to="1"></li>
        <li data-target="#slides" data-slide-to="2"></li>
        <li data-target="#slides" data-slide-to="3"></li>
    </ol>

    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-slide-1.jpg" alt="...">
        </div>
        <div class="item">
            <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-slide-2.jpg" alt="...">
        </div>
        <div class="item">
            <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-slide-3.jpg" alt="...">
        </div>
        <div class="item">
            <img src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-slide-4.jpg" alt="...">
        </div>
    </div>

    <a class="left carousel-control" href="#slides" role="button" data-slide="prev">
        <span class="icon-prev" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#slides" role="button" data-slide="next">
        <span class="icon-next" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <img id="bottom-slide" src="<?php echo get_home_url(); ?>/wp-content/themes/the-landscaper-child/assets/images/fn-web-07.png">
</div>