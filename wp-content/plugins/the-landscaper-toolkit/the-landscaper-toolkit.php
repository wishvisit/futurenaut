<?php
/**
 *	Plugin Name: The Landscaper Toolkit
 *	Plugin URI: http://www.qreativethemes.com
 *  Description: All plugin territory functionalities for The Landscaper WordPress Theme, by QreativeThemes
 *	Version: 1.1
 *	Author: QreativeThemes
 *	Author URI: http://www.qreativethemes.com
 *	Text Domain: thelandscaper-toolkit
 *	Domain Path: /languages/
 *
 *	@package thelandscaper-toolkit
 *	@author QreativeThemes
 *
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * The core plugin class.
 */
if ( ! class_exists( 'TheLandscaperToolkit' ) ) {
	class TheLandscaperToolkit {

		public function __construct() {

			// Get all required files
			$this->thelandscaper_toolkit_include_files();
			
			// Load the translation file
			$this->thelandscaper_toolkit_load_textdomain();

			// Deactivate QreativeShortcodes plugin if this plugin is activated
			add_action( 'admin_init', array( $this, 'thelandscaper_toolkit_on_activation' ) );
		}

		/**
		 * Include all required files
		 */
		private function thelandscaper_toolkit_include_files() {

			// Get the custom ACF PRO fields
			require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . 'includes/acf/acf-fields.php' );

			// Get custom widgets
			require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . 'includes/widgets/widget-init.php' );

			// Get shortcodes
			require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . 'includes/shortcodes/shortcodes.php' );
		}

		/**
		 * Load text domain for translation
		 */
		public function thelandscaper_toolkit_load_textdomain() {
			load_plugin_textdomain( 'thelandscaper-toolkit', false, trailingslashit( plugin_dir_path( __FILE__ ) ) . 'languages/' ); 
		}

		/**
		 * De-activate the QreativeShortcodes plugin on activation of The Landscaper Toolkit plugin
		 * The shortcodes are moved to this plugin so QreativeShortcodes is not necessary anymore
		 */
		public function thelandscaper_toolkit_on_activation() {

			if ( class_exists( 'QreativeShortcodes' ) ) {
				deactivate_plugins( 'qreativeshortcodes/qreativeshortcodes.php' );
			}
		}
	}
	new TheLandscaperToolkit();
}