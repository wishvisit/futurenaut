<?php
/**
 * Get and register custom widgets
 *
 * @package thelandscaper-toolkit
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Get all widget files
$thelandscaper_toolkit_get_widgets = array(
	'widget-brochure',
	'widget-count-box',
	'widget-cta-banner',
	'widget-cta-button',
	'widget-facebook',
	'widget-featured-page',
	'widget-google-map',
	'widget-icon-box',
	'widget-opening-hours',
	'widget-recent-posts-block',
	'widget-recent-posts-list',
	'widget-social-icons',
	'widget-team-member',
	'widget-testimonials'
);

foreach ( $thelandscaper_toolkit_get_widgets as $widget_file ) {
	require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . 'files/' . $widget_file . '.php' );
}

// Register all widgets
if ( ! function_exists( 'thelandscaper_toolkit_register_widgets' ) ) {
	function thelandscaper_toolkit_register_widgets() {

		// All widgets have the QT_ prefix because the theme is used since 2015 with this prefix
		// Changing this prefix is not possible and will break all widgets on customer websites

		// Define all theme widgets
		$thelandscaper_toolkit_register_widgets = apply_filters( 'thelandscaper-toolkit-register-widgets', array(
			'QT_Brochure',
			'QT_Count_Box',
			'QT_CTA_Banner',
			'QT_CTA_Button',
			'QT_Feature_Page',
			'QT_Google_Map',
			'QT_Icon_Box',
			'QT_Opening_Hours',
			'QT_Recent_Posts_Block',
			'QT_Recent_Posts',
			'QT_Social_Icons',
			'QT_Team_Member',
			'QT_Testimonials',
			'QT_Facebook'
		) );

		foreach ( $thelandscaper_toolkit_register_widgets as $widget_name ) {
			register_widget( $widget_name );
		}
	}
	add_action( 'widgets_init', 'thelandscaper_toolkit_register_widgets' );
}